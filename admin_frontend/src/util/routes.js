
import Login from '../components/Login';
import Dashboard from '../components/Dashboard';
import Home from "../components/Home";
import Category from "../components/Category";
import AddForm from '../components/Category/addForm';
import CategoriesList from '../components/Category/list';
import SubCategory from '../components/SubCategories';
import AddSubCategoryForm from '../components/SubCategories/addSubCategoryForm';
import SubCategoriesList from '../components/SubCategories/list';
import AddProduct from '../components/Product/add';
import ProductList from '../components/Product/list';
import ProductDetail from '../components/Product/detail';
import EditProduct from '../components/Product/edit';
import EditCategoryForm from '../components/Category/edit';
import EditSubCategoryForm from '../components/SubCategories/edit';
import CustomerList from '../components/Customers/list';
import CustomerDetails from '../components/Customers/details';
import OrderList from '../components/Orders/list';
import OrderDetail from '../components/Orders/details';



export const routes = [

  {
    path : "/admin",
    component : Dashboard,
    routes : [
     
      {
            path: "/admin/orders/list/view/:orderid",
            component : OrderDetail
      },
      {
        path: "/admin/orders/list",
        component : OrderList
      },
        {
            path: "/admin/customers/list",
            component : CustomerList
       },
       {
            path: "/admin/customers/list/view/:customerid",
            component : CustomerDetails
      },
      {
        path: "/admin/products/list/edit/:productid",
        component : EditProduct
      },
      {
        path: "/admin/products/list/view/:productid",
        component : ProductDetail
      },
      {
        path: "/admin/products/add",
        component : AddProduct
      },
      {
        path: "/admin/products/list",
        component : ProductList
      },
      {
        path: "/admin/categories/list/edit/:cat_id",
        component : EditCategoryForm
      },
      {
        path: "/admin/categories/list/view/:cat_id",
        component : EditCategoryForm
      },
      {
        path: "/admin/categories/list",
        component : CategoriesList
      },
      {
        path: "/admin/categories/add",
        component : AddForm
      },
      {
        path: "/admin/categories",
        component: Category
      },
      {
        path: "/admin/sub-categories/add",
        component: AddSubCategoryForm
      },
      {
        path: "/admin/sub-categories/list/edit/:sub_cat_id",
        component : EditSubCategoryForm
      },
      {
        path: "/admin/sub-categories/list",
        component: SubCategoriesList
      },
      {
        path: "/admin/sub-categories",
        component: SubCategory
      },
      
      {
        path: "/",
        component: Home
      },
    ]
  },
  {
    path : "/sign-in",
    component : Login
  },
  {
    path: "/",
    component: Login
  },
];

