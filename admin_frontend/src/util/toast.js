import React ,{ useState} from "react";
import { Toast } from "react-bootstrap"


const ToastInfo = (props) => {

    return(

        <Toast  show={props.visiblestatus}  style={{
            position: 'fixed',
            top: 10,
            right: 10,
            minWidth: 300,
            minHeight : 70,
            zIndex: 999999,
            'background-color': 'rgba(0,204,204,1.85)'
          }}>
            <Toast.Body >
                {props.content}
            </Toast.Body>
            
          </Toast>

    )

}

export default ToastInfo;