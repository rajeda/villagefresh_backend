export const SERVER_URL = (window.location.hostname === "localhost") ? "http://localhost:5000/api/" : "https://www.villagefresh-admin.xyz/api/";

export const SERVER_IMG_URL = (window.location.hostname === "localhost") ? "http://localhost:5000/" : "https://www.villagefresh-admin.xyz/";
