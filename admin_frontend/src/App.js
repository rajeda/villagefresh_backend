import React,{useContext} from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import BaseProvider from "./BaseProvider"

import { routes } from "./util/routes";
import Header from './components/Includes/header';


function RouteWithSubRoutes(route) {
  return (
    <Route
      path={route.path}
      render={props => (
        // pass the sub-routes down to keep nesting
        <route.component {...props} routes={route.routes} prop={props} />
      )}
    />
  );
}

function App() {


  return (
    <BaseProvider>

  
    <Router>
      <div className="container-fluid">
        <div className="row ">
          




          <Header />

          <div className="col-md-12">
    
            <Switch >
            {routes.map((route, i) => (
                <RouteWithSubRoutes key={i} {...route} />
              ))}
            </Switch>


          </div>
        </div>

      </div>
    </Router>

    </BaseProvider>
  );
}

export default App;
