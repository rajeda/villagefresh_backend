import { createContext } from "react";
// this is the same to the createStore method of Redux
const BaseContext = createContext();

export default BaseContext;