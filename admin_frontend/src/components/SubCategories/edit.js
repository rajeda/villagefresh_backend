import React,{useState,useEffect,useContext} from "react";
import { SERVER_URL } from "../../config";
import axios from "axios";
import { useParams } from "react-router-dom";
import { Card ,Breadcrumb } from "react-bootstrap"
import { Link } from "react-router-dom"
import BaseContext from "../../BaseContext"


const EditSubCategoryForm = (props) => {

    const {sub_cat_id} = useParams();
    const [category,setCategory] = useState([]);
    const [subcategory,setsubCategory] = useState("");
    const [updated_category,setUpdateCategory] = useState("");
    const [category_error,setCategoryError] = useState("");
    const [sub_category_error,setSubCategoryError] = useState("");
    const rootState = useContext(BaseContext)



    const updateSubCategoryInputValue = (e) => {
        setsubCategory(e.target.value);
    };

    const updateCategoryChangeValue = (e) => {
        setUpdateCategory(e.target.value);
    };
    const updateSubCategory = () => {

        setCategoryError("");
        setSubCategoryError("");

        if(updated_category === "") {
            setCategoryError("Category Required");
            return;
        }
        if(subcategory === "") {
            setSubCategoryError("Sub Category Required");
            return;
        }


        let url = SERVER_URL+"sub-categories";
        let formdata = {sub_cat_id : sub_cat_id,sub_category_name : subcategory , category : updated_category };
        let login_item = localStorage.getItem("login");
        login_item = JSON.parse(login_item);
        let auth_token = login_item.token;


        axios.put(url,formdata,{
            headers : {
                "auth-key" : auth_token
            }
        })
            .then((res) => {
                rootState.updateToast("Sub Category Updated Successfully.",true);
                props.history.push("/admin/sub-categories/list")
            })
            .catch(err => {
                console.log("err",err);
            });


    };

    useEffect(() => {

        const loadSubcategory = async () => {
            let login_item = localStorage.getItem("login");
            login_item = JSON.parse(login_item);
            let auth_token = login_item.token;
            var subcategory = await axios.get(`${SERVER_URL}sub-categories/fetch-one/${sub_cat_id}`,{
                headers : {
                    "auth-key" : auth_token
                }
            });
            subcategory = subcategory.data;
            if(subcategory.status === 1){
                setsubCategory(subcategory.data.sub_category_name);
                setUpdateCategory(subcategory.data.category);
            }
        }
        async function callAjax() {
            var response = await loadCategories();
            response = response.data;
            console.log(response)

            if(response.status === 1){
                setCategory(response.data);
            }
        }

        callAjax();
        loadSubcategory();
    },[sub_cat_id]);

    const loadCategories = () => {

        let login_item = localStorage.getItem("login");
        login_item = JSON.parse(login_item);
        let auth_token = login_item.token;
        return  axios.get(SERVER_URL+"categories",{
            headers : {
                "auth-key" : auth_token
            }
        });
    }

    // const loadSubcategory = async () => {
    //     let login_item = localStorage.getItem("login");
    //     login_item = JSON.parse(login_item);
    //     let auth_token = login_item.token;
    //     var subcategory = await axios.get(`${SERVER_URL}sub-categories/${sub_cat_id}`,{
    //         headers : {
    //             "auth-key" : auth_token
    //         }
    //     });
    //     subcategory = subcategory.data;
    //     if(subcategory.status === 1){
    //         setsubCategory(subcategory.data.sub_category_name);
    //         setUpdateCategory(subcategory.data.category);
    //     }
    // }

    return(

        <div>

<Breadcrumb>
                <Breadcrumb.Item><Link to="/admin">Dashboard</Link></Breadcrumb.Item>
                <Breadcrumb.Item>
                    <Link to="/admin/sub-categories/list">Sub Categories</Link>
                </Breadcrumb.Item>
                    <Breadcrumb.Item active>Edit</Breadcrumb.Item>
                </Breadcrumb>

                   <Card>
                <Card.Header>Sub Categories Edit</Card.Header>
                <Card.Body>

        
        
            <div className="form-group">
                <label>Category:</label>
                <select className="form-control" value={updated_category} onChange={updateCategoryChangeValue}>
                    <option value="">Select Category</option>
                    {
                        category && category.map((val,index) => {
                            return <option key={index} value={val._id}>{val.category_name}</option>
                        })
                    }
                </select>
                <span className="text-danger">{category_error}</span>

            </div>
            <div className="form-group">
                <label>Sub Category Name:</label>
                <input type="text" name="category_name"  className="form-control" id="category_name" value={subcategory}
                    onChange={updateSubCategoryInputValue}
                />
                <span className="text-danger">{sub_category_error}</span>

            </div>

            <button className="btn btn-primary" onClick={updateSubCategory}>Save</button>


            </Card.Body>
                </Card>
        </div>
    )
}

export default EditSubCategoryForm;