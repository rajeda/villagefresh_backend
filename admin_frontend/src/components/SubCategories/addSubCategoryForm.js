import React,{useState,useEffect,useContext} from "react";
import { SERVER_URL } from "../../config";
import axios from "axios";
import { Card ,Breadcrumb } from "react-bootstrap"
import { Link } from "react-router-dom"
import BaseContext from "../../BaseContext"

const AddSubCategoryForm = (props) => {

    const [category,setCategory] = useState([]);
    const [subcategory,setsubCategory] = useState("");
    const [category_error,setCategoryError] = useState("");
    const [sub_category_error,setSubCategoryError] = useState("");

    const [updated_category,setUpdateCategory] = useState("");
    const rootState = useContext(BaseContext)

    const updateSubCategoryInputValue = (e) => {
        setsubCategory(e.target.value);
    };

    const updateCategoryChangeValue = (e) => {
        setUpdateCategory(e.target.value);
    };
    const createSubCategory = () => {
        setCategoryError("");
        setSubCategoryError("");

        if(updated_category === "") {
            setCategoryError("Category Required");
            return;
        }
        if(subcategory === "") {
            setSubCategoryError("Sub Category Required");
            return;
        }

        let url = SERVER_URL+"sub-categories";
        let formdata = {sub_category_name : subcategory , category : updated_category };

        let login_item = localStorage.getItem("login");
        login_item = JSON.parse(login_item);
        let auth_token = login_item.token;


        axios.post(url,formdata,{
            headers : {
                "auth-key" : auth_token
            }
            })
            .then((res) => {
                
                rootState.updateToast("Sub Category Added Successfully.",true);
                props.history.push("/admin/sub-categories/list")
            })
            .catch(err => {
                console.log("err",err);
            });


    };

    useEffect(() => {


        async function callAjax() {
            var response = await loadCategories();
            response = response.data;
            console.log(response)

            if(response.status === 1){
                setCategory(response.data);
            }
        }

        callAjax();

    },[]);

    const loadCategories = () => {

        let login_item = localStorage.getItem("login");
        login_item = JSON.parse(login_item);
        let auth_token = login_item.token;



        return  axios.get(SERVER_URL+"categories",{
            headers : {
                "auth-key" : auth_token
            }
        });
    }

    return(

        <div>

                <Breadcrumb>
                <Breadcrumb.Item><Link to="/admin">Dashboard</Link></Breadcrumb.Item>
                <Breadcrumb.Item>
                    <Link to="/admin/sub-categories/list">Sub Categories</Link>
                </Breadcrumb.Item>
                    <Breadcrumb.Item active>Add</Breadcrumb.Item>
                </Breadcrumb>

                   <Card>
                <Card.Header>Sub Categories Add</Card.Header>
                <Card.Body>
                   
            <div className="form-group">
                <label>Category:</label>
                <select className="form-control" onChange={updateCategoryChangeValue}>
                    <option>Select Category</option>
                    {
                        category && category.map((val,index) => {
                            return <option key={index} value={val._id}>{val.category_name}</option>
                        })
                    }
                </select>
                <span className="text-danger">{category_error}</span>
            </div>
            <div className="form-group">
                <label>Sub Category Name:</label>
                <input type="text" name="category_name"  className="form-control" id="category_name" value={subcategory}
                    onChange={updateSubCategoryInputValue}
                />
                <span className="text-danger">{sub_category_error}</span>

            </div>

            <button className="btn btn-primary" onClick={createSubCategory}>Save</button>

            </Card.Body>
                </Card>

 

        </div>
    )
}

export default AddSubCategoryForm;