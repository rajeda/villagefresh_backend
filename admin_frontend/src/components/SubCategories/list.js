import React,{useEffect,useState,useContext} from "react";
import { SERVER_URL } from "../../config";
import axios from "axios";
import { Link } from "react-router-dom";
import { Card ,Breadcrumb  } from "react-bootstrap"
import BaseContext from "../../BaseContext"

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPencilAlt , faTrash
} from '@fortawesome/free-solid-svg-icons'

const getSubCategoryCount =  () => {

        
    let login_item = localStorage.getItem("login");
    login_item = JSON.parse(login_item);
    let auth_token = login_item.token;
    return  axios.get(SERVER_URL+"sub-categories/fetch-count",{
        headers : {
            "auth-key" : auth_token
        }
    });


}

const SubCategoriesList = () => {

    const [subcategories, setSubCategories] = useState([]);
    const [skip , setSkip ] = useState(0);
    const [limit, setLimit] = useState(10);
    const [count , setCount ] = useState(0);
    const [disable_btn , setDisable ] = useState(false);
    const [count_paginate , setPaginateCount ] = useState(1);
    const rootState = useContext(BaseContext)

    useEffect(  () => {

        var response = loadContent(skip,limit);
        response.then(
            (res) => {
                response = res.data;
                if(response.status === 1){
                    setSubCategories(response.data);
   
                
                }
            }
        )

        var response_total = getSubCategoryCount();
        response_total.then(
            (res) => {
                response = res.data;
                if(response.status === 1){
                    setCount(response.data);
                }
            }
        )


    }, [skip,limit])

    async function getSubcategories() {
        var response = await loadContent();
        response = response.data;
        if(response.status === 1){
            
            setSubCategories(response.data);
   
        }
    }

    const loadContent = (skip,limit) => {

        let login_item = localStorage.getItem("login");
        login_item = JSON.parse(login_item);
        let auth_token = login_item.token;

        rootState.updateProgressBar()
        //rootState.setProgressBarPercentage(80)

        return  axios.get(SERVER_URL+"sub-categories/fetch-all/"+skip+"/"+limit,{

            headers : {
                "auth-key" : auth_token
            },
            onUploadProgress: function (progressEvent) {
               // rootState.setProgressBarPercentage(80)
                // Do whatever you want with the native progress event
              },
            onDownloadProgress: progressEvent => {



            }
              
        }
        );
    }

    const remove = async (id) => {

        var confirm = window.confirm("Are you sure wanted to remove?");

        if(confirm) {
            
            let login_item = localStorage.getItem("login");
            login_item = JSON.parse(login_item);
            let auth_token = login_item.token;


            var remove_subcategory = await axios.delete(`${SERVER_URL}sub-categories/${id}`,{
                headers : {
                    "auth-key" : auth_token
                        }
            });
            remove_subcategory = remove_subcategory.data;
            if(remove_subcategory.status === 1){
                rootState.updateToast("Sub Category Deleted Successfully.",true);
                getSubcategories();
            }
        }
    }

    const previousPage = () => {

        
        setDisable(false);
        if(skip <= limit) {
            setPaginateCount(1);
        }
        setSkip(skip-limit)


    }

    const nextPage = () => {


        let total_count =  Math.round(count / limit);
        setSkip(skip+limit)
        setPaginateCount(count_paginate + 1);

        if(count_paginate === total_count) {
            setDisable(true)
        }
    }
   

   // if(loader){

        return(


            <div className={ rootState.progressBarPercentage === 0 ? "" : "overlay"} >

                    <Breadcrumb>
                <Breadcrumb.Item><Link to="/admin">Dashboard</Link></Breadcrumb.Item>
                <Breadcrumb.Item>
                    <Link to="/admin/sub-categories/list">Sub Categories</Link>
                </Breadcrumb.Item>
                    <Breadcrumb.Item active>List</Breadcrumb.Item>
                </Breadcrumb>

                <Card>
                <Card.Header>Sub Categories List
                <Link className="btn btn-primary float-right" to="/admin/sub-categories/add">Add</Link>

                </Card.Header>
                <Card.Body>
                   

                        <table className="table ">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Sub Category Name</th>
                                <th>Category</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                subcategories && subcategories.map( (val,index) => {
                                    return(
                                        <tr key={index}>
                                            <td>{index+skip+1}</td>
                                            <td>{val.sub_category_name}</td>
                                            <td>{val.category.category_name}</td>
                                            <td>
                                                <Link className="btn btn-dark" to={`/admin/sub-categories/list/edit/${val._id}`}>
                                                <FontAwesomeIcon icon={faPencilAlt}  />
                                                </Link>
                                                <button className="ml-2 btn btn-danger" onClick={
                                                    () => {
            
                                                        remove(val._id)
                                                    }
                                                }>
                                                 <FontAwesomeIcon icon={faTrash}  />
                                                </button>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
            
                    <div>
                        <button className="float-left" onClick={previousPage} disabled={ skip <= 0}>Previous Page</button>
                        <button className="float-right" onClick={nextPage}  disabled={  disable_btn }>Next Page</button>
                    </div>
                </Card.Body>
                </Card>

 

            </div>

        )



    // }else{

    //     return(


    //         <div className="col-md-12  vertical-center">
                
    //             <Button variant="primary" disabled>
    //                 <Spinner
    //                 as="span"
    //                 animation="grow"
    //                 size="sm"
    //                 role="status"
    //                 aria-hidden="true"
    //                 />
    //                 Loading...
    //             </Button>
    //         </div>


    //     )
    // }

       

    
    
};


export default SubCategoriesList;


