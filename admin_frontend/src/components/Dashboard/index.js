import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
    
  } from "react-router-dom";
import Header from "../Includes/header";
import Sidebar from "../Includes/sidebar";




function RouteWithSubRoutes(route) {


    
  return (
    <Route
      path={route.path}
      render={props => (
        // pass the sub-routes down to keep nesting
        <route.component {...props} routes={route.routes}  />
      )}
    />
  );


  

  }

const Dashboard = ( { routes , prop }) => {

  



    
  
    let login_item = localStorage.getItem("login");
    if(login_item === null || login_item === ""){
      return <Redirect to='/' />
    }

    return(
        <Router>
            <Header />
          <div className="row" id="body-row">
            
            <Sidebar />

            <div className="col py-3">

                <Switch >
                      {routes.map((route, i) => (
                          <RouteWithSubRoutes key={i} {...route}  />
                      ))}
                </Switch>

            </div>

          </div>

        </Router>

    )
}

export default Dashboard;