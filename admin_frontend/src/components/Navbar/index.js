import React from 'react';
import { Link  } from "react-router-dom";

 
const Navbar = () => {



    return(

      <div className="nav flex-column flex-nowrap vh-100 overflow-auto text-white p-2">
        <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
     
            <li>
            <a href="/#">Categories</a>
                <ul>
                  <li>
                    <Link to="/admin/categories/add">Add</Link>
                  </li>
              
                  <li>
                    <Link to="/admin/categories/list">list</Link>
                  </li>
                </ul>
            </li>

            <li>
            <a href="/#">Sub Categories</a>
                <ul>
                  <li>
                    <Link to="/admin/sub-categories/add">Add</Link>
                  </li>
            
                  <li>
                    <Link to="/admin/sub-categories/list">list</Link>
                  </li>
                </ul>
            </li>

            <li>
              <a href="/#">Products</a>
                <ul>
                  <li>
                    <Link to="/admin/products/add">Add</Link>
                  </li>
                  <li>
                    <Link to="/admin/products/list">list</Link>
                  </li>
                </ul>
            </li>

            <li>
              <a href="/#">Customers</a>
                <ul>
                  <li>
                    <Link to="/admin/customers/list">list</Link>
                  </li>
                </ul>
            </li>


            <li>
              <a href="/#">Orders</a>
                <ul>
                  <li>
                    <Link to="/admin/orders/list">list</Link>
                  </li>
                </ul>
            </li>

          </ul>
      </div>
    );

};

export default Navbar;