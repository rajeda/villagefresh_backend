import React , { useEffect , useState , useContext}from "react";
import axios from "axios";
import { SERVER_URL } from "../../config";
import { Link } from "react-router-dom";
import { Card ,Breadcrumb  } from "react-bootstrap"
import BaseContext from "../../BaseContext"

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPencilAlt , faTrash , faSearch
} from '@fortawesome/free-solid-svg-icons'


const getProductsCount =  () => {

        
    let login_item = localStorage.getItem("login");
    login_item = JSON.parse(login_item);
    let auth_token = login_item.token;
    return  axios.get(SERVER_URL+"products/count",{
        headers : {
            "auth-key" : auth_token
        }
    });


}


const ProductList = () => {


    const [products, setProducts] = useState([]);
    const rootState = useContext(BaseContext)
    const [skip , setSkip ] = useState(0);
    const [limit, setLimit] = useState(5);
    const [count , setCount ] = useState(0);
    const [disable_btn , setDisable ] = useState(false);
    const [count_paginate , setPaginateCount ] = useState(1);

    useEffect(
        () => {

                
            var response_total = getProductsCount();
            response_total.then(
                (res) => {
                    let response = res.data;
                    if(response.status === 1){
                        setCount(response.data);
                    }
                }
            )



            var products_response = loadProducts();
            products_response.then(
                (res) => {

                    products_response = res.data;
                    if(products_response.status === 1){
                        setProducts(products_response.data);
                    }

                }
            )    
        },
        [skip,limit]
    );

    
    async function getProducts() {

        var products_response = await loadProducts();
        products_response = products_response.data;
        if(products_response.status === 1){
            setProducts(products_response.data);
        }

    }
    function loadProducts() {
        let login_item = localStorage.getItem("login");
        login_item = JSON.parse(login_item);
        let auth_token = login_item.token;

        rootState.updateProgressBar()

        return axios.get(SERVER_URL+"products",{
            params : {
                skip,limit
            },
            headers : {
                "auth-key" : auth_token
                    }
        });
    }

    const removeProduct = async (id) => {
        var confirm = window.confirm("Are you sure wanted to remove?");

        if(confirm) {
            let login_item = localStorage.getItem("login");
            login_item = JSON.parse(login_item);
            let auth_token = login_item.token;
            var remove_product = await axios.delete(`${SERVER_URL}products/${id}`,{
                headers : {
                    "auth-key" : auth_token
                        }
            });
            remove_product = remove_product.data;
            if(remove_product.status === 1){
                getProducts();
            }
        }    
    };

    const previousPage = () => {

        
        setDisable(false);
        if(skip <= limit) {
            setPaginateCount(1);
        }
        setSkip(skip-limit)


    }

    const nextPage = () => {


        let total_count =  Math.round(count / limit);
        setSkip(skip+limit)
        setPaginateCount(count_paginate + 1);

        if(count_paginate === total_count) {
            setDisable(true)
        }
    }

    return (

        <div>


            <Breadcrumb>
            <Breadcrumb.Item><Link to="/admin">Dashboard</Link></Breadcrumb.Item>
            <Breadcrumb.Item>
                    <Link to="/admin/products/list">Products</Link>
            </Breadcrumb.Item>
                    <Breadcrumb.Item active>List</Breadcrumb.Item>
            </Breadcrumb>

            <Card>
            <Card.Header>Products List
            <Link className="btn btn-primary float-right" to="/admin/products/add">Add</Link>

            </Card.Header>
            <Card.Body>



            <table className="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Product Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        products && products.map( (val,index) => {
                            return(
                                <tr key={index}>
                                    <td>{index+skip+1}</td>
                                    <td>{val.title}</td>
                                    <td>
                                        <Link className="btn btn-warning" to={`/admin/products/list/view/${val._id}`} >
                                        <FontAwesomeIcon icon={faSearch }  />
                                        </Link>
                                        <Link className="btn btn-dark ml-2" to={`/admin/products/list/edit/${val._id}`} >
                                        <FontAwesomeIcon icon={faPencilAlt}  />
                                        </Link>
                                        <button className="ml-2 btn btn-danger" 
                                        onClick={() => {

                                            removeProduct(val._id);
                                        }}
                                        >
                                            <FontAwesomeIcon icon={faTrash}  />
                                        </button>

                                    </td>
                                </tr>
                            )
                        })
                        }
                </tbody>
            </table>
            <div>
                        <button className="float-left" onClick={previousPage} disabled={ skip <= 0}>Previous Page</button>
                        <button className="float-right" onClick={nextPage}  disabled={  disable_btn }>Next Page</button>
                    </div>

            </Card.Body>
            </Card>
        </div>
    )


};


export default ProductList;