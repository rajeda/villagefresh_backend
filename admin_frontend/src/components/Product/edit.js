import React,{useState,useEffect,useContext} from "react";
import axios from "axios";
import { SERVER_URL , SERVER_IMG_URL } from "../../config";
import {useParams} from "react-router-dom";
import { Card ,Breadcrumb  } from "react-bootstrap"
import { Link } from "react-router-dom";
import BaseContext from "../../BaseContext"

const EditProduct = (props) => {

    const { productid } = useParams();

    const [category,setCategory] = useState([]);
    const [subcategory,setsubCategory] = useState([]);
    const [category_changed_val,setChangedCategory] = useState("");
    const [sub_category_changed_val,setSubChangedCategory] = useState("");
    const [title,setTitle] = useState("");
    const [sku,setSku] = useState("");
    const [description,setDescription] = useState("");
    const [d_information,setDInformation] = useState("");
    const [base_price,setBPrice] = useState("");
    const [offer_price,setOPrice] = useState("");
    const [quantiy_available,setQuantity] = useState("");
    const [images,setImages] = useState([]);
    const [image_html,setImageHtml] = useState([]);
    const [image_uploaded_content,setUploadedImageContent] = useState([]);
    const rootState = useContext(BaseContext)


    //errors
    const [title_error,setTitleError] = useState("");
    const [category_error,setCategoryError] = useState("");
    const [sub_category_error,setSubCategoryError] = useState("");
    const [base_price_error,setBasePriceError] = useState("");
    const [offer_price_error,setOfferPriceError] = useState("");
    const [quantity_error,setQuantityError] = useState("");
    const [image_error,setImageError] = useState("");

    const validateProductForm = () => {

        let status = true;
    
        if(title === ""){
            setTitleError("Title Required");
            status = false;
        }
    
        if(category_changed_val === ""){
            setCategoryError("Category Required");
            status = false;
        }
    
        if(sub_category_changed_val === ""){
            setSubCategoryError("Sub Category Required");
            status = false;
        }

        if(base_price === ""){
            setBasePriceError("Base Price Required");
            status = false;
        }

        
        if(offer_price === ""){
            setOfferPriceError("Offer Price Required");
            status = false;
        }
    
        if(quantiy_available === ""){
            setQuantityError("Quantity Required");
            status = false;
        }

        // if(images.length === 0){
        //     setImageError("Upload Atlease One Product Image");
        //     status = false;
        // }

        return status;
    };

    useEffect(
        () => {


            callComponentLoadAjax();

            
            async function callComponentLoadAjax() {

                var product_response = await loadProductDetails(productid);
                product_response = product_response.data;
                if(product_response.status === 1){

                    let product_cat_id = product_response.data.category._id;
                    setTitle(product_response.data.title);
                    setSku(product_response.data.sku);
                    setDescription(product_response.data.description);
                    setBPrice(product_response.data.base_price);
                    setOPrice(product_response.data.offer_price);
                    setQuantity(product_response.data.quantiy_available);
                    setChangedCategory(product_cat_id);
                    setSubChangedCategory(product_response.data.sub_category._id);
                    setDInformation(product_response.data.delivery_information);

                    //product image
                    if(product_response.data.images){
                        var data = []
                        product_response.data.images.map(val => {
                            return data.push(val)
                        });
                        setUploadedImageContent(data);

                    }

                    //Category and subcategory loading
                    var cat_response = await loadCategories();
                    cat_response = cat_response.data;
                    if(cat_response.status === 1){
                        setCategory(cat_response.data);
                        
                        var sub_cat_response = await loadSubCategories(product_cat_id);
                        sub_cat_response = sub_cat_response.data;
                        if(sub_cat_response.status === 1){
                            setsubCategory(sub_cat_response.data);
                        }
                    }

                }
            }


        },
        [productid]
    )


    function loadProductDetails(productid) {
        let login_item = localStorage.getItem("login");
        login_item = JSON.parse(login_item);
        let auth_token = login_item.token;

        return axios.get(SERVER_URL+"products/"+productid,{
            headers : {
                "auth-key" : auth_token
                    }
        });
    }

    function loadCategories() {
        let login_item = localStorage.getItem("login");
        login_item = JSON.parse(login_item);
        let auth_token = login_item.token;
        return  axios.get(SERVER_URL+"categories",{
            headers : {
                "auth-key" : auth_token
                    }
        });

    }

    function loadSubCategories(cat_id) {
        let login_item = localStorage.getItem("login");
        login_item = JSON.parse(login_item);
        let auth_token = login_item.token;
        return  axios.get(SERVER_URL+"categories/sub-categories/list/"+cat_id,{
            headers : {
                "auth-key" : auth_token
                    }
        });

    }

 
    async function updateCategoryChangeValue(e) {

        setChangedCategory(e.target.value);
        var sub_cat_response = await loadSubCategories(e.target.value);
        sub_cat_response = sub_cat_response.data;
        if(sub_cat_response.status === 1){
            setsubCategory(sub_cat_response.data);

        }
    }

    function updateSubCategoryChangeValue(e) {
        setSubChangedCategory(e.target.value);
    }

    function updateImageContent(e){
        e.preventDefault();
        let cotnent_length = image_html.length + 1;
        console.log(cotnent_length)
        setImageHtml(image_html => [...image_html, cotnent_length]);

    }


    async function saveProduct(e) {
        e.preventDefault() // Stop form submit



        if(!validateProductForm()){
            return;
        }


        var formData = new FormData();

        if(images.length > 0) {
            images.map((val,index) => {
               return formData.append('product_images',val);
            })
        }
        
        formData.append('product_id',productid);
        formData.append('title',title);
        formData.append('sku',sku);
        formData.append('description',description);
        formData.append('category',category_changed_val);
        formData.append('sub_category',sub_category_changed_val);
        formData.append('delivery_information',d_information);
        formData.append('quantiy_available',quantiy_available);
        formData.append('base_price',base_price);
        formData.append('offer_price',offer_price);

        var url = SERVER_URL+"products";

        let login_item = localStorage.getItem("login");
        login_item = JSON.parse(login_item);
        let auth_token = login_item.token;


        const config = { 
            headers: { 'Content-Type': 'multipart/form-data', "auth-key" : auth_token },
            onUploadProgress: function (progressEvent) {
                rootState.updateProgressBar();
                console.log('dd',progressEvent)
            },
            onDownloadProgress: progressEvent => {
                var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
            }
        };
        

        var createproduct = await axios.put(url,formData,config);

        var createproduct_response = createproduct.data;
        if(createproduct_response.status === 1){
            rootState.updateToast("Product Updated Successfully.",true);
            props.history.push("/admin/products/list")
        }
        return false;
    }

    

    async function removeImage(productid,ind) {

        var confirm = window.confirm("Are you sure wanted to remove image?")

        if(confirm){

            var formdata = {
                product_id : productid,
                index : ind
            }
            var remove = await axios.post(SERVER_URL+"products/imageremove",formdata);
            var remove_response = remove.data;
            if(remove_response.status === 1){
                var data = []
                remove_response.data.images.map(val => {
                    return data.push(val)
                });
                setUploadedImageContent(data);
                alert("Successfully removed");
            }

        }
       

        return false;
    }

    return(

        <div>




<Breadcrumb>
            <Breadcrumb.Item><Link to="/admin">Dashboard</Link></Breadcrumb.Item>
            <Breadcrumb.Item>
                    <Link to="/admin/products/list">Products</Link>
            </Breadcrumb.Item>
                    <Breadcrumb.Item active>Edit</Breadcrumb.Item>
            </Breadcrumb>

            <Card>
            <Card.Header>Products Edit</Card.Header>
            <Card.Body>
            <form onSubmit={saveProduct} action="#" method="post" encType="multipart/form-data">
            <div className="form-group">
                <label>Title <span className="text-danger">*</span></label>
                <input type="text" placeholder="Product Title" className="form-control"  value={title}
                        onChange={(e) => {
                            setTitle(e.target.value);
                        }}
                    />
                                    <span className="text-danger">{title_error}</span>

            </div>

            <div className="form-group">
                <label>Sku</label>
                <input type="text" placeholder="Product SKU" className="form-control"  value={sku}
                        onChange={(e) => {
                            setSku(e.target.value);
                        }}
                    />
            </div>

            <div className="form-group">
                <label>Description</label>
                <textarea  placeholder="Product Description" className="form-control"  
                        onChange={(e) => {
                            setDescription(e.target.value);
                        }}
                        defaultValue={description}
                ></textarea>
            </div>



            <div className="form-group">
                <label>Category <span className="text-danger">*</span></label>
                <select className="form-control" onChange={updateCategoryChangeValue}  value={category_changed_val}>
                    <option value="">Select Category</option>
                    {
                        category && category.map((val,index) => {
                            return <option key={index} value={val._id}
                            >{val.category_name}</option>
                        })
                    }
                </select>
                <span className="text-danger">{category_error}</span>

            </div>
        
            <div className="form-group">
                <label>Sub Category <span className="text-danger">*</span></label>
                <select className="form-control" onChange={updateSubCategoryChangeValue} value={sub_category_changed_val}>
                    <option value="">Select Sub Category</option>
                    {
                        subcategory && subcategory.map((val,index) => {
                            return <option key={index} value={val._id}
                           
                            >{val.sub_category_name}</option>
                        })
                    }
                </select>
                <span className="text-danger">{sub_category_error}</span>
            </div>



            <div className="form-group">
                <label>Delivery Information</label>
                <input type="text" placeholder="Product Delivery Information" className="form-control"  value={d_information}
                        onChange={(e) => {
                            setDInformation(e.target.value);
                        }}
                    />
            </div>


            <div className="form-group">
                <label>Base Price <span className="text-danger">*</span></label>
                <input type="text" placeholder="Product Base Price" className="form-control"  value={base_price}
                        onChange={(e) => {
                            setBPrice(e.target.value);
                        }}
                    />
                    <span className="text-danger">{base_price_error}</span>
            </div>




            <div className="form-group">
                <label>Offer Price <span className="text-danger">*</span></label>
                <input type="text" placeholder="Product Offer Price" className="form-control"  value={offer_price}
                        onChange={(e) => {
                            setOPrice(e.target.value);
                        }}
                    />
                    <span className="text-danger">{offer_price_error}</span>
            </div>

            <div className="form-group">
                <label>Quantity Available <span className="text-danger">*</span></label>
                <input type="text" placeholder="Product Quantity" className="form-control"  value={quantiy_available}
                        onChange={(e) => {
                            setQuantity(e.target.value);
                        }}
                    />
                    <span className="text-danger">{quantity_error}</span>
            </div>

            <div className="form-group">
            <p className="card card-body bg-light"><b>Images</b> <br />
                                {image_uploaded_content && image_uploaded_content.map((val,index) => {

                                    var name = SERVER_IMG_URL+val.image_folder+"/"+ val.image_name;
                                    return(
                                    <span key={index}>
                                        <img src={name}  alt="Product Images"/> 
                                        <a href="/#" onClick={
                                            () => {
                                               return removeImage(productid,index) 
                                            }
                                        }
                                        
                                        >Delete</a> <br/>
                                    </span>
                                    )
                                })}
                            </p>

            </div>

            <div className="form-group">
                <label>Images</label>
                <input type="file" className="form-control" 
                        onChange={(e) => {
                            images.push(e.target.files[0]);
                                        setImages(images);
                        }}
                    />
                
            </div>

            <div className="form-group">
            <label></label>
            <button onClick={updateImageContent}>Add More</button>
            </div>

            {
                image_html && image_html.map((val,index) => {
                    return (
                        <div className="form-group" key={val}>
                            <label>Images</label>
                            <input type="file" className="form-control" 
                                    onChange={(e) => {
                                        images.push(e.target.files[0]);
                                        setImages(images);
                                    }}
                                />
                            
                            <button className="btn btn-danger" onClick={(event) => {

                                        event.preventDefault();
                                        var new_image_html = image_html.filter( (img) => {
                                                if(img !== val) {
                                                    return img; 
                                                }else{
                                                    return []; 
                                                }
                                        });
                                        setImageHtml(new_image_html);

                            }}>X</button>
                        </div>
                    )
                 }
                )
            }

          

            <button className="btn btn-primary" type="submit" >Save</button>
            </form>

            </Card.Body>
            </Card>
    </div>

    );
};

export default EditProduct;