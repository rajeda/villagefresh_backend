import React , { useState,useContext } from "react";
import { SERVER_URL } from "../../config";
import axios from "axios";
import { Redirect } from "react-router-dom";
import BaseContext from "../../BaseContext"
import ToastInfo from "../../util/toast";

  

const Login = (props) => {

    const [email,setEmail] = useState("rajeshedayangat@gmail.com");
    const [password,setPassword] = useState("123456");
    const rootState = useContext(BaseContext)
    const [status,setStatus] = useState(false);


    const submitForm = async (e) => {

        e.preventDefault();


        rootState.updateProgressBar()

        let submitForm = await axios.post(`${SERVER_URL}users/login`,{email,password});

        submitForm = submitForm.data;
        if(submitForm.status === 1){
           
           localStorage.setItem(
               'login',
               JSON.stringify({
                   login : true,
                   token : submitForm.data
               })
           )
           rootState.updateToast("Succesfully Logined",true);

            setStatus(true)
        }else{
            alert(submitForm.message)
        }
    };


    let storage_content = localStorage.getItem("login");

    if(storage_content){

        return <Redirect to="/admin/" />
    }else{

        
    return(

        <div>

        <ToastInfo content="This HTML file is a template. test" visiblestatus={status} />

        <div className="row mx-md-n5" style={{"marginTop" : "100px"}}>
            <div className="col px-md-5" style={{"minHeight" : "400px"}}>
                <div className="p-3 border bg-light">
                    <h1>Village Fres Admin</h1>    
                </div>
            </div>
            <div className="col px-md-5" style={{"minHeight" : "400px"}}>
                <div className="p-3 border bg-light">
                    <form onSubmit={submitForm}>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Email address</label>
                            <input type="email" value={email}  onChange={
                                (e) => {
                                    setEmail(e.target.value)
                                }
                            } className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                            <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Password</label>
                            <input type="password" value={password} onChange={
                                (e) => {
                                    setPassword(e.target.value)
                                }
                            } className="form-control" id="exampleInputPassword1" />
                        </div>
                        <div className="form-group form-check">
                            <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                            <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        </div>

    )


    }

    



};

export default Login;