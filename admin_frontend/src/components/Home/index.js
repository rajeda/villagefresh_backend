import React , {useContext,useState,useEffect} from 'react';
import { Link } from "react-router-dom"
import BaseContext from "../../BaseContext"
import "./style.css"
import { SERVER_URL } from "../../config";
import axios from "axios";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingBasket ,faArrowCircleRight,faUsers
} from '@fortawesome/free-solid-svg-icons'

const getOrderssCount =  () => {

        
  let login_item = localStorage.getItem("login");
  login_item = JSON.parse(login_item);
  let auth_token = login_item.token;
  return  axios.get(SERVER_URL+"orders/order/count",{
      headers : {
          "auth-key" : auth_token
      }
  });


}



const getCustomerCount =  () => {

        
  let login_item = localStorage.getItem("login");
  login_item = JSON.parse(login_item);
  let auth_token = login_item.token;
  return  axios.get(SERVER_URL+"customers/customer/count",{
      headers : {
          "auth-key" : auth_token
      }
  });


}
const Home = () => {


    const rootState = useContext(BaseContext);
    const [orders , setOrder ] = useState(0);
    const [customers, setCustomer] = useState(0);
    useEffect(
      () => {

      var response_total = getOrderssCount();
      rootState.updateProgressBar()

      response_total.then(
          (res) => {
              let response = res.data;
              if(response.status === 1){
                setOrder(response.data);
              }
          }
      )

      var response_total = getCustomerCount();
      rootState.updateProgressBar()

      response_total.then(
          (res) => {
              let response = res.data;
              if(response.status === 1){
                setCustomer(response.data);
              }
          }
      )



  },[orders,customers])

    return(
      <div className="row">
      <div className="col-lg-2 col-sm-6">
            <div className="circle-tile ">
              <a href="#"><div className="circle-tile-heading dark-blue">
                <FontAwesomeIcon icon={faShoppingBasket} size="fa-fw fa-3x" />
                </div></a>
              <div className="circle-tile-content dark-blue">
                <div className="circle-tile-description text-faded"> Orders</div>
                <div className="circle-tile-number text-faded ">{orders}</div>
                <Link className="circle-tile-footer" to="/admin/orders/list">More Info
                <FontAwesomeIcon icon={faArrowCircleRight}  />
                </Link>
              </div>
            </div>
          </div>
          
          <div className="col-lg-2 col-sm-6">
            <div className="circle-tile ">
              <a href="#"><div className="circle-tile-heading red">
              <FontAwesomeIcon icon={faUsers} size="fa-fw fa-3x" />                
              </div></a>
              <div className="circle-tile-content red">
                <div className="circle-tile-description text-faded"> Customers </div>
                <div className="circle-tile-number text-faded ">{customers}</div>
                <Link className="circle-tile-footer" to="/admin/customers/list">More Info
                <FontAwesomeIcon icon={faArrowCircleRight}  />                
                </Link>
              </div>
            </div>
          </div> 


      </div>



    )
  
  };

  
  export default Home;