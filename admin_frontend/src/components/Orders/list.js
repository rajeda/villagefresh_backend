import React ,{useState,useEffect,useContext} from "react";
import axios from "axios";
import { SERVER_URL } from "../../config";
import { Link } from "react-router-dom";
import { Card ,Breadcrumb  } from "react-bootstrap"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch
} from '@fortawesome/free-solid-svg-icons'
import BaseContext from "../../BaseContext"

const getOrderssCount =  () => {

        
    let login_item = localStorage.getItem("login");
    login_item = JSON.parse(login_item);
    let auth_token = login_item.token;
    return  axios.get(SERVER_URL+"orders/order/count",{
        headers : {
            "auth-key" : auth_token
        }
    });


}


const OrderList = () => {

    const [orders,SetOrders] = useState([])
    const rootState = useContext(BaseContext)
    const [skip , setSkip ] = useState(0);
    const [limit, setLimit] = useState(5);
    const [count , setCount ] = useState(0);
    const [disable_btn , setDisable ] = useState(false);
    const [count_paginate , setPaginateCount ] = useState(1);

    useEffect(
        () => {

        var response_total = getOrderssCount();
        response_total.then(
            (res) => {
                let response = res.data;
                if(response.status === 1){
                        setCount(response.data);
                }
            }
        )



        let login_item = localStorage.getItem("login");
        login_item = JSON.parse(login_item);
        let auth_token = login_item.token;

        rootState.updateProgressBar()

        axios.get(SERVER_URL+"orders",{
            params : {
                skip,limit
            },
            headers : {
                "auth-key" : auth_token
                    }
        }).then(
            (res) => {

                let order_response = res.data;
                if(order_response.status === 1){
                    SetOrders(order_response.data)
                }
            }
        )
        .catch(err => {
                alert("failed to load")
                console.log("errr")
            }
        );
       


    },[skip,limit])


    const previousPage = () => {

        
        setDisable(false);
        if(skip <= limit) {
            setPaginateCount(1);
        }
        setSkip(skip-limit)


    }

    const nextPage = () => {


        let total_count =  Math.round(count / limit);
        setSkip(skip+limit)
        setPaginateCount(count_paginate + 1);

        if(count_paginate === total_count) {
            setDisable(true)
        }
    }

    return (
        <div>

<Breadcrumb>
            <Breadcrumb.Item><Link to="/admin">Dashboard</Link></Breadcrumb.Item>
            <Breadcrumb.Item>
                    <Link to="/admin/orders/list">Orders</Link>
            </Breadcrumb.Item>
                    <Breadcrumb.Item active>List</Breadcrumb.Item>
            </Breadcrumb>

            <Card>
            <Card.Header>Orders List</Card.Header>
            <Card.Body>
        <table className="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Mobile Number</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {
                    orders && orders.map( (val,index) => {
                        return(
                            <tr key={index}>
                                <td>{index+skip+1}</td>
                                <td>{val._id}</td>
                                <td>{val.customer.first_name} {val.customer.last_name}</td>
                                <td>{val.customer.mobile}</td>
                                <td><span className="badge badge-info">{val.order_status}</span></td>

                                <td>
                                <Link className="btn btn-warning" to={`/admin/orders/list/view/${val._id}`} >
                                <FontAwesomeIcon icon={faSearch }  />
                                </Link>
                                </td>
                            </tr>
                        )
                    })
                }
            </tbody>
        </table>
        <div>
                        <button className="float-left" onClick={previousPage} disabled={ skip <= 0} >Previous Page</button>
                        <button className="float-right" onClick={nextPage}  disabled={  disable_btn }>Next Page</button>
                    </div>
        </Card.Body>
            </Card>

        </div>
    )

}


export default OrderList



