import React ,{useState,useEffect,useContext}from "react"
import {useParams} from "react-router-dom";
import axios from "axios";
import { SERVER_URL } from "../../config";
import './style.css'
import { Link } from "react-router-dom";
import { Card ,Breadcrumb  } from "react-bootstrap"

import BaseContext from "../../BaseContext"


const OrderDetail = () => {
    const rootState = useContext(BaseContext);

    const {orderid} = useParams();
    const [order,setOrder] = useState({});
    useEffect(
        () => {
            const getOrderDetails = async () => {
                try{
                    let login_item = localStorage.getItem("login");
                    login_item = JSON.parse(login_item);
                    let auth_token = login_item.token;
                    let order_details = await axios.get(SERVER_URL + "orders/" + orderid,{
                        headers : {
                            "auth-key" : auth_token
                                }
                    });
                    order_details = order_details.data;
                    

                    let initialstate = {
                        customer_name : order_details.data.customer.first_name + " " + order_details.data.customer.last_name,
                        customer_email : order_details.data.customer.email,
                        customer_mobile : order_details.data.customer.mobile,
                        order_id : order_details.data._id,
                        order_date : order_details.data.modified_on,
                        order_status :order_details.data.order_status,
                        billing : order_details.data.billing_address,
                        shipping : order_details.data.shipping_address,
                        payment_method : order_details.data.payment_method,
                        payment_status : order_details.data.payment_status,
                        payable_amount : order_details.data.order_total,
                        total_amount : order_details.data.order_total,
                        paid_amount : order_details.data.order_total,
                        products : order_details.data.products,

                    };
                    console.log("initialstate",initialstate)
                    setOrder(initialstate);


                }catch(error){
                    alert("failed to load");
                }


            }
            getOrderDetails();

        },
        [orderid]
    );


    const updateOrderStatus = (e) => {

        let url = SERVER_URL+"orders/update-status";
        let formdata = {status : e.target.value , order_id : orderid };

                    
        let login_item = localStorage.getItem("login");
        login_item = JSON.parse(login_item);
        let auth_token = login_item.token;
        rootState.updateProgressBar()

        axios.post(url,formdata,{
                headers : {
                    "auth-key" : auth_token
                }
            })
            .then((res) => {
                console.log("Res",res);
                rootState.updateToast("Order Status Updated Successfully.",true);
            })
            .catch(err => {
                console.log("err",err);
            });

        
    }
    if( Object.keys(order).length === 0 && order.constructor === Object){

        return(
            <div>
                <p>Loading...</p>
            </div>
        )

    }else{


        return (

            <div>

            <Breadcrumb>
            <Breadcrumb.Item><Link to="/admin">Dashboard</Link></Breadcrumb.Item>
            <Breadcrumb.Item>
                    <Link to="/admin/orders/list">Orders</Link>
            </Breadcrumb.Item>
                    <Breadcrumb.Item active>List</Breadcrumb.Item>
            </Breadcrumb>

            <Card>
            <Card.Header>Orders List</Card.Header>
            <Card.Body>


            <div className="card">
                <div className="card-header bg-secondary">
                View Order
                </div>
                <div className="card-body">
                    <div className="row">

                        <div className="col-md-6">
                            <div className="float-left">
                            <div className="row"><label>Order Id</label> : {order.order_id}</div>
                            <div className="row"><label>Order Date</label> : {order.order_date}</div>
                            <div className="row"><label>Status</label> : <span className="badge badge-warning">{order.order_status}</span>
                                <select defaultValue={order.order_status} onChange={updateOrderStatus}>
                                    <option value="">Update Status</option>
                                    <option value="Delivered">Delivered</option>
                                    <option value="On The Way">On The Way</option>
                                    <option value="Pending">Pending</option>
                                    <option value="Approved">Approved</option>
                                    <option value="Cancel">Cancel</option>
                                </select>
                            </div>
                            </div>

                        </div>
                        <div className="col-md-6">

                        <div className="float-right">
                            <div className="row"><label>Customer Details</label></div>
                            <div className="row"><label>Name</label> : {order.customer_name}</div>
                            <div className="row"><label>Email</label> : {order.customer_email}</div>
                            <div className="row"><label>Mobile</label> : {order.customer_mobile}</div>
                        </div>
                        </div>


                    </div>
    
                    <div className="row">
                        <div className="col-md-12">
                            <div class="card">
                                <div class="card-header bg-secondary">
                                    Payment & Shipping Details
                                </div>
                                <div class="card-body">
                                    
                                    <div className="row">
                                        <p>Payment Details : -</p>
                                    </div>
    
                                    <div className="row">
                                        <div className="col-md-4">
                                             <label>Payment Type : </label>{order.payment_method}
                                        </div>
                                        <div className="col-md-4">
                                            <label>Payment Status : </label>{order.payment_status}
                                        </div>
                                        <div className="col-md-4">
                                            <label>Payable Amount : </label> INR {order.payable_amount}
                                        </div>
                                        <div className="col-md-4">
                                            <label>Total Amount : </label>{order.total_amount}
                                        </div>
                                        <div className="col-md-4">
                                            <label>Paid Amount : </label>{order.paid_amount}
                                        </div>
    
                                    </div>
                                    <div className="row"><p>Billing Address : -</p></div>

                                    <div className="row">
                                     
                                        <div className="col-md-4">
                                            <label>First Name  :</label> {order.billing.first_name} 
                                        </div>
                                        <div className="col-md-4">
                                            <label>Last Name : </label>{order.billing.last_name} 
                                        </div>
                                        <div className="col-md-4">
                                            <label>House Name : </label>{order.billing.house_name}
                                        </div>
                                        <div className="col-md-4">
                                            <label>Location :</label> {order.billing.location}
                                        </div>


                          

                                        <div className="col-md-4">
                                            <label>Street :</label> {order.billing.street}
                                        </div>
                                        <div className="col-md-4">
                                            <label>Pincode : </label>{order.billing.pincode}
                                        </div>
                                        <div className="col-md-4">
                                            <label>City : </label>{order.billing.city}
                                        </div>
                                        
                                    </div>
    
                                    <div className="row">
                                        <p>Shipping  Address : -</p>
                                    </div>
    
                                    <div className="row">
                                     
                                     <div className="col-md-4">
                                         <label>First Name  :</label> {order.shipping.first_name} 
                                     </div>
                                     <div className="col-md-4">
                                         <label>Last Name : </label> {order.shipping.last_name}
                                     </div>
                                     <div className="col-md-4">
                                         <label>House Name : </label>{order.shipping.house_name}
                                     </div>
                                     <div className="col-md-4">
                                         <label>Location :</label> {order.shipping.location}
                                     </div>



                                     <div className="col-md-4">
                                         <label>Street :</label> {order.shipping.street}
                                     </div>
                                     <div className="col-md-4">
                                         <label>Pincode :</label> {order.shipping.pincode}
                                     </div>
                                     <div className="col-md-4">
                                         <label>City : </label>{order.shipping.city}
                                     </div>
                                     
                                 </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row" style={{marginTop:"10px"}}>
                        <div className="col-md-12">
                            <div class="card">
                                <div class="card-header bg-secondary">
                                    Order Detail
                                </div>
                                <div class="card-body">
                                   <table className="table table-striped  ">
                                       <thead className="thead-dark">

                                            <tr>
                                                <th>Title</th>
                                                <th>Unit Price</th>
                                                <th>Quantity</th>
                                                <th>Total Amount</th>
                                            </tr>

                                       </thead>

                                       <tbody>
                                           {
                                               order.products && order.products.map((val) => {
                                                   return(
                                                       <tr>
                                                           <td>{val.name}</td>
                                                           <td>{val.price}</td>
                                                           <td>{val.quantity}</td>
                                                           <td>INR {val.quantity * val.price}</td>
                                                       </tr>
                                                   )
                                               })
                                           }
                                       </tbody>

                                       <tfoot >
                                           <tr >
                                               <td colSpan="2" ></td>
                                               <td >Total</td>
                                               <td>INR {order.total_amount}</td>
                                           </tr>
                                           <tr>
                                                <td colSpan="2" ></td>
                                               <td>Tax</td>
                                               <td>INR 0.00</td>
                                           </tr>
                                           <tr>
                                                <td colSpan="2" ></td>
                                               <td>Grand Total</td>
                                               <td>INR {order.total_amount}</td>
                                           </tr>
                                       </tfoot>

                                   </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            


            </Card.Body>
            </Card>

            </div>





        )



    }

   

}


export default OrderDetail