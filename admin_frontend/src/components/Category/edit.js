import React , { useEffect , useState , useContext}from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { SERVER_URL } from "../../config";
import { Link } from "react-router-dom";
import { Card ,Breadcrumb  } from "react-bootstrap"
import BaseContext from "../../BaseContext"
import ToastInfo from "../../util/toast";

const EditCategoryForm = (props) => {

    const { cat_id } = useParams();
    const [category,setCategory] = useState("");
    const [category_error,setCatError] = useState("");
    const rootState = useContext(BaseContext)

    const updateCategory = async (e) => {

        e.preventDefault();

        if(category === ""){
            setCatError("Category name requried");
            return;
        }

        const formdata = {
            cat_id : cat_id,
            category_name : category
        };


        let login_item = localStorage.getItem("login");
        login_item = JSON.parse(login_item);
        let auth_token = login_item.token;


        var category_update = await axios.put(`${SERVER_URL}categories`,formdata,{
                    headers : {
                        "auth-key" : auth_token
                    }
        });

        
        if(category_update){
            category_update = category_update.data;
            if(category_update.status === 1) {

                rootState.updateToast("Succesfully Updated Category",true);
                props.history.push("/admin/categories/list");
            } else{

                rootState.updateToast("Failed To Update Category",true);

            }      
        }
    };

    useEffect(
        () => {
            const callAjax = async () => {

                                            
                let login_item = localStorage.getItem("login");
                login_item = JSON.parse(login_item);
                let auth_token = login_item.token;


                let category = await axios.get(`${SERVER_URL}categories/${cat_id}`,{
                    headers : {
                        "auth-key" : auth_token
                    }
                });
                if(category){
                    category = category.data;
                    if(category.status === 1) {
                        setCategory(category.data.category_name);
                    }
                }
            }

            callAjax();
        },
        [cat_id]
    )
    return(

        <div>

            <Breadcrumb>
            <Breadcrumb.Item><Link to="/admin">Dashboard</Link></Breadcrumb.Item>
            <Breadcrumb.Item>
                    <Link to="/admin/categories/list">Categories</Link>
            </Breadcrumb.Item>
                    <Breadcrumb.Item active>Edit</Breadcrumb.Item>
            </Breadcrumb>

            <Card>
            <Card.Header>Categories Edit</Card.Header>
            <Card.Body>

                           
            <div className="row" >

            <div className="col-md-6">

                <div className="form-group">
                    <label>Category Name:</label>
                    <input type="text" name="category_name" className="form-control" id="category_name" value={category}
                        onChange={(e) => {
                            setCategory(e.target.value);
                        }}
                    />
                    
                    <span className="text-danger">{category_error}</span>

                </div>

                <button className="btn btn-info" onClick={updateCategory}>Save</button>

            </div>
            <div className="col-md-6"></div>

            </div>


        </Card.Body>
            </Card>
    </div>
    )
};

export default EditCategoryForm;