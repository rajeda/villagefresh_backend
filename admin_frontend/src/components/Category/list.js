import React,{useEffect,useState,useContext} from "react";
import { SERVER_URL } from "../../config";
import axios from "axios";
import { Link } from "react-router-dom";
import { Card ,Breadcrumb  } from "react-bootstrap"
import BaseContext from "../../BaseContext"

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPencilAlt , faTrash
} from '@fortawesome/free-solid-svg-icons'


const getCategoryCount =  () => {

        
    let login_item = localStorage.getItem("login");
    login_item = JSON.parse(login_item);
    let auth_token = login_item.token;
    return  axios.get(SERVER_URL+"categories/count",{
        headers : {
            "auth-key" : auth_token
        }
    });


}


const CategoriesList = (props) => {

    const [categories, setCategories] = useState([]);
    const rootState = useContext(BaseContext)
    const [skip , setSkip ] = useState(0);
    const [limit, setLimit] = useState(5);
    const [count , setCount ] = useState(0);
    const [disable_btn , setDisable ] = useState(false);
    const [count_paginate , setPaginateCount ] = useState(1);

   // const [token,setToken] = useState("")

    useEffect(  () => {


       // setToken(login_item.token);

        var response_total = getCategoryCount();
        response_total.then(
            (res) => {
                let response = res.data;
                if(response.status === 1){
                    setCount(response.data);
                }
            }
        )

        getCategories();


    }, [limit,skip])

    const loadContent = (auth_token) => {
        rootState.updateProgressBar()

        return  axios.get(SERVER_URL+"categories",{
            params : {
                skip,limit
            },
            headers : {
                "auth-key" : auth_token
            }
        });
    }

    async function getCategories() {
        
            
        let login_item = localStorage.getItem("login");
        login_item = JSON.parse(login_item);


        var response = await loadContent(login_item.token);
        response = response.data;
        if(response.status === 1){
            setCategories(response.data);
        }
    }


    const removeCategory = async (cat_id) => {

        var confirm =window.confirm("Are you sure wanted to remove it ? . Please make sure to remove all subcategories associates with category.");

        if(confirm){
            let login_item = localStorage.getItem("login");
            login_item = JSON.parse(login_item);
            let auth_token = login_item.token;
    

            var remove_category = await axios.delete(`${SERVER_URL}categories/${cat_id}`,{
                headers : {
                    "auth-key" : auth_token
                }
            });
            if(remove_category){
                remove_category = remove_category.data;
                if(remove_category.status === 1){
                   // alert("successfully deleted");
                    rootState.updateToast("Category Deleted Successfully.",true);
                    getCategories();
                }
            }
        }

    }


    
    const previousPage = () => {

        
        setDisable(false);
        if(skip <= limit) {
            setPaginateCount(1);
        }
        setSkip(skip-limit)


    }

    const nextPage = () => {


        let total_count =  Math.round(count / limit);
        setSkip(skip+limit)
        setPaginateCount(count_paginate + 1);

        if(count_paginate === total_count) {
            setDisable(true)
        }
    }
   

    return(

        <div>


            <Breadcrumb>
            <Breadcrumb.Item><Link to="/admin">Dashboard</Link></Breadcrumb.Item>
            <Breadcrumb.Item>
                    <Link to="/admin/categories/list">Categories</Link>
            </Breadcrumb.Item>
                    <Breadcrumb.Item active>List</Breadcrumb.Item>
            </Breadcrumb>

            <Card>
            <Card.Header>Categories List
                <Link className="btn btn-primary float-right" to="/admin/categories/add">Add</Link>
            </Card.Header>
            <Card.Body>

                <table className="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Category Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            categories && categories.map( (val,index) => {
                                return(
                                    <tr key={index}>
                                        <td>{index+skip+1}</td>
                                        <td>{val.category_name}</td>
                                        <td>
                                        <Link className="btn btn-dark" to={`/admin/categories/list/edit/${val._id}`} >
                                        <FontAwesomeIcon icon={faPencilAlt}  />
                                        </Link>
                                        <button  className="ml-2 btn btn-danger" onClick={
                                            (e) => {
                                                removeCategory(val._id);
                                            }
                                        }>
                                         <FontAwesomeIcon icon={faTrash}  />
                                        </button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>

                            
                    <div>
                        <button className="float-left" onClick={previousPage} disabled={ skip <= 0}>Previous Page</button>
                        <button className="float-right" onClick={nextPage}  disabled={  disable_btn }>Next Page</button>
                    </div>

            </Card.Body>
            </Card>
        </div>
       
    )
};


export default CategoriesList;


