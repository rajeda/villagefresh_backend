import React,{useState,useContext} from "react";
import { SERVER_URL } from "../../config";
import axios from "axios";
import { Card ,Breadcrumb } from "react-bootstrap"
import { Link } from "react-router-dom"
import BaseContext from "../../BaseContext"


const AddForm = (props) => {

    const [category,setCategory] = useState("");
    const [category_error,setCatError] = useState("");

    const rootState = useContext(BaseContext)

    const updateCategoryInputValue = (e) => {
        setCategory(e.target.value);
    };

    const createCategory = () => {

        if(category === "") {
            setCatError("Category name requried");
            return;
        }
        let url = SERVER_URL+"categories";
        let formdata = {category_name : category };

                    
        let login_item = localStorage.getItem("login");
        login_item = JSON.parse(login_item);
        let auth_token = login_item.token;

        axios.post(url,formdata,{
                headers : {
                    "auth-key" : auth_token
                }
            })
            .then((res) => {
               // console.log("Res",res);
                rootState.updateToast("Category Added Successfully.",true);
                props.history.push("/admin/categories/list")
               // alert("Succfully added");
            })
            .catch(err => {
                console.log("err",err);
            });


    };

    return(

        <div>

                <Breadcrumb>
                <Breadcrumb.Item><Link to="/admin">Dashboard</Link></Breadcrumb.Item>
                <Breadcrumb.Item>
                    <Link to="/admin/categories/list">Categories</Link>
                </Breadcrumb.Item>
                    <Breadcrumb.Item active>Add</Breadcrumb.Item>
                </Breadcrumb>

                   <Card>
                <Card.Header>Categories Add</Card.Header>
                <Card.Body>
           
                <div className="row" >

                    <div className="col-md-6">

                        <div className="form-group">
                                <label>Category Name:</label>
                                <input type="text" className="form-control" name="category_name" id="category_name" value={category}
                                    onChange={updateCategoryInputValue}
                                />
                                <span className="text-danger">{category_error}</span>

                            </div>

                            <button className="btn btn-primary" onClick={createCategory}>Save</button>

                    </div>

                    
                    <div className="col-md-6">
                        
                    </div>




                </div>



            </Card.Body>
                </Card>
        </div>
    )
}

export default AddForm;