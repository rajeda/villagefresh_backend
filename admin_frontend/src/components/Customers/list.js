import React , { useEffect , useState , useContext }from "react";
import { SERVER_URL } from "../../config";
import axios from "axios";
import { Link } from "react-router-dom";
import { Card ,Breadcrumb  } from "react-bootstrap"
import BaseContext from "../../BaseContext"


import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch
} from '@fortawesome/free-solid-svg-icons'



const getCustomerCount =  () => {

        
    let login_item = localStorage.getItem("login");
    login_item = JSON.parse(login_item);
    let auth_token = login_item.token;
    return  axios.get(SERVER_URL+"customers/customer/count",{
        headers : {
            "auth-key" : auth_token
        }
    });


}

const CustomerList  = () => {

    const [customers, setCustomers ] = useState([]);
    const rootState = useContext(BaseContext)
    const [skip , setSkip ] = useState(0);
    const [limit, setLimit] = useState(10);
    const [count , setCount ] = useState(0);
    const [disable_btn , setDisable ] = useState(false);
    const [count_paginate , setPaginateCount ] = useState(1);
    // const [token,setToken] = useState("")
 
     useEffect(  () => {
 
 
        // setToken(login_item.token);
 
 
         async function callAjax() {
         
                             
            var response_total = getCustomerCount();
            response_total.then(
                (res) => {
                    let response = res.data;
                    if(response.status === 1){
                        setCount(response.data);
                    }
                }
            )



             let login_item = localStorage.getItem("login");
             login_item = JSON.parse(login_item);
 
 
             var response = await loadContent(login_item.token);
             response = response.data;
             if(response.status === 1){
                setCustomers(response.data);
             }
         }
 
         callAjax();
 
 
     }, [skip,limit])
 
     const loadContent = (auth_token) => {
        rootState.updateProgressBar()

         return  axios.get(SERVER_URL+"customers",{
            params : {
                skip,limit
            },
             headers : {
                 "auth-key" : auth_token
             }
         });
     }

     const previousPage = () => {

        
        setDisable(false);
        if(skip <= limit) {
            setPaginateCount(1);
        }
        setSkip(skip-limit)


    }

    const nextPage = () => {


        let total_count =  Math.round(count / limit);
        setSkip(skip+limit)
        setPaginateCount(count_paginate + 1);

        if(count_paginate === total_count) {
            setDisable(true)
        }
    }


    return(

        <div>

<Breadcrumb>
            <Breadcrumb.Item><Link to="/admin">Dashboard</Link></Breadcrumb.Item>
            <Breadcrumb.Item>
                    <Link to="/admin/customers/list">Customers</Link>
            </Breadcrumb.Item>
                    <Breadcrumb.Item active>List</Breadcrumb.Item>
            </Breadcrumb>

            <Card>
            <Card.Header>Customers List</Card.Header>
            <Card.Body>


       
        <table className="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Contact Number</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            {
                customers && customers.map( (val,index) => {
                    return(
                        <tr key={index}>
                            <td>{index+skip+1}</td>
                            <td>{val.first_name} {val.last_name}</td>
                            <td>{val.mobile}</td>
                            <td>{val.email}</td>

                            <td>
                            <Link className="btn btn-warning" to={`/admin/customers/list/view/${val._id}`} >
                            <FontAwesomeIcon icon={faSearch }  />
                            </Link>
                            </td>
                        </tr>
                    )
                })
            }
        </tbody>
    </table>

    <div>
                        <button className="float-left" onClick={previousPage} disabled={ skip <= 0}>Previous Page</button>
                        <button className="float-right" onClick={nextPage}  disabled={  disable_btn }>Next Page</button>
                    </div>
    </Card.Body>
            </Card>
    </div>
    )

};

export default CustomerList;