import React from "react"
import { Link } from "react-router-dom";
import { Card ,Breadcrumb  } from "react-bootstrap"

const CustomerDetails  = () => {

    

    return(
        <div>
        <Breadcrumb>
            <Breadcrumb.Item><Link to="/admin">Dashboard</Link></Breadcrumb.Item>
            <Breadcrumb.Item>
                    <Link to="/admin/customers/list">Customers</Link>
            </Breadcrumb.Item>
                    <Breadcrumb.Item active>List</Breadcrumb.Item>
            </Breadcrumb>

            <Card>
            <Card.Header>Customers List</Card.Header>
            <Card.Body>


            <h1>Customer Detail</h1>

            </Card.Body>
            </Card>
        </div>
    )

};

export default CustomerDetails;