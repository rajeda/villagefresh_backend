import React from "react";
import { Link  } from "react-router-dom";
import {Accordion,Card,Button} from "react-bootstrap"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome , faBriefcase , faUsers , faShoppingBasket , faCity ,
faListAlt
} from '@fortawesome/free-solid-svg-icons'



const Sidebar = () => {


    return(

        <div id="sidebar-container" className="sidebar-expanded d-none d-md-block col-2">
              <ul className="list-group sticky-top sticky-offset">

                    <Link to="/admin/" className="text-light bg-dark list-group-item list-group-item-action flex-column align-items-star">Dashboard <FontAwesomeIcon icon={faHome}  /></Link> 
                    <Link to="/admin/categories/list" className="text-light bg-dark list-group-item list-group-item-action flex-column align-items-star">Categories <FontAwesomeIcon icon={faCity}  /></Link> 
                    <Link to="/admin/sub-categories/list" className="text-light bg-dark list-group-item list-group-item-action flex-column align-items-star">Sub Categories <FontAwesomeIcon icon={faListAlt}  /> </Link> 
                    <Link to="/admin/products/list" className="text-light bg-dark list-group-item list-group-item-action flex-column align-items-star">Products <FontAwesomeIcon icon={faBriefcase}  /> </Link> 
                    <Link to="/admin/customers/list" className="text-light bg-dark list-group-item list-group-item-action flex-column align-items-star">Customers <FontAwesomeIcon icon={faUsers}  /> </Link> 
                    <Link to="/admin/orders/list" className="text-light bg-dark list-group-item list-group-item-action flex-column align-items-star">Orders <FontAwesomeIcon icon={faShoppingBasket}  /> </Link> 

            
                 
              </ul>
          </div>
    )


};

export default Sidebar;