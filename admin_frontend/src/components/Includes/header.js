import React ,{useState,useContext}from "react";
import { ProgressBar  , Navbar , Nav , NavDropdown   ,Toast } from "react-bootstrap"
import BaseContext from "../../BaseContext"

const Header = (prop) => {

    const rootState = useContext(BaseContext)
    let auth_token = "";
    const useSignOut = () => {
        rootState.updateProgressBar()
        rootState.updateToken()
        localStorage.clear();
        //prop.history.push("/");
        window.location.href="/";
    };

    let login_item = localStorage.getItem("login");


    if(login_item){

        login_item = JSON.parse(login_item);

         auth_token = login_item.token;
    
    }


    return(
        <div>

   <div>
   

            <Navbar bg="dark" variant="dark" expand="lg" fixed="top">
            <Navbar.Brand href="#home">Village Fresh</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                {/* <Nav.Link href="#home">Home</Nav.Link>
                <Nav.Link href="#link">Link</Nav.Link> */}

              

                </Nav>

                {
                    (auth_token == "" ) ? "" : (
                        <NavDropdown title="Hi Admin,"  id="basic-nav-dropdown" >
                        <NavDropdown.Item href="#action/3.1">Profile</NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.3"  onClick={useSignOut}>Logout</NavDropdown.Item>
                    
                         </NavDropdown>
                    )
                }

 

  
            </Navbar.Collapse>
            </Navbar>



        </div>

        <div>
                    
        <div className={
            `${rootState.progressBarDisplay ? "hide_progress" : "show_progress"}
             ${ true ? "fixed-top" : ""}
            `
        } >
                        
                <ProgressBar   now={rootState.progressBarPercentage} variant="success" min={0} max={100} style={{height : '5px'}}  />      

         </div>

       

        </div>
</div>        
    )
};

export default Header;