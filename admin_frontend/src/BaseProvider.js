import BaseContext from './BaseContext'
import React,{useState,useEffect} from "react"
import ToastInfo from './util/toast';



const BaseProvider = (props) => {

        const [progressBarDisplay,setProgressBarDisplay] = useState(false);
        const [progressBarPercentage,setProgressBarPercentage] = useState(0);
        const [toastDisplayStatus,setToastDisplay] = useState(false);
        const [toastDisplayContent,setToastDisplayContent] = useState("");

                            


        const [authToken,setAUthToken] = useState("");

        useEffect(
            () => {

                let login_item = localStorage.getItem("login");
                if(login_item){

                    login_item = JSON.parse(login_item);
            
                    let auth_token = login_item.token;
    
                    setAUthToken(auth_token)

                }

            },
            [authToken]
        )
            const updateToken = () => {
                
                setAUthToken("")
            }

    const updateToast = (content,status) => {

        setToastDisplayContent(content);
        setToastDisplay(status);

        setTimeout(() => {
            setToastDisplayContent("");
            setToastDisplay(false)
        },3000)
    }

    const updateProgressBar = () => {


        setProgressBarDisplay(false)
        setProgressBarPercentage(0)
        var i=0;
        var interv = setInterval(function(){
              setProgressBarPercentage(i)
              i = i+ 25;
              if(i > 100) {
                    clearInterval(interv);
                    setProgressBarDisplay(true)
                    setTimeout(() => {
                        setProgressBarPercentage(0)
                    },600)
               }
         } , 50);

    }
    return(

        <BaseContext.Provider
            value={{
                toastDisplayStatus,
                toastDisplayContent,
                progressBarDisplay,
                progressBarPercentage,
                authToken,
                setProgressBarDisplay,
                setProgressBarPercentage,
                updateProgressBar,
                updateToast,
                updateToken
            }}>
                    
           <ToastInfo content={toastDisplayContent} visiblestatus={toastDisplayStatus} />

            {props.children}
        </BaseContext.Provider>

    )
}



export default BaseProvider;