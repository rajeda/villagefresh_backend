const mongoose  = require("mongoose");

const categorySchema = mongoose.Schema({

    category_name : {
        type : String,
        required : true
    },
    created : {
        type : Date,
        default : Date.now
    },
    updated : {
        type : Date
    }

});

module.exports = mongoose.model("Category",categorySchema)