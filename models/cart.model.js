const mongoose = require("mongoose");


const cartSchema = mongoose.Schema({

    user_id : {
        type : mongoose.Schema.Types.ObjectId,
        required : true,
        ref : 'User'
    },
    products : [
        {
            product_id : String,
            quantity : Number,
            name : String,
            price : Number
        }
    ],
    active : {
        type : Boolean,
        default : true
    },
    modified_on : {
        type  : Date,
        default : Date.now()
    }
},

{ timestamps: true }

);


module.exports = mongoose.model('Cart',cartSchema);