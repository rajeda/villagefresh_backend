const mongoose = require("mongoose")


const userSchema = mongoose.Schema({
    username : {
        type : String,
        reuired : true
    },
    email : {
        type : String,
        required : true
    },
    password : {
        type :String,
        required : true
    },
    active : {
        type : Boolean,
        default : true
    },
    token_id : {
        type : String
    }

});


module.exports = mongoose.model("User",userSchema)
