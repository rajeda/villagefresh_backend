const mongoose = require("mongoose");


const orderSchema = mongoose.Schema({

    customer : {
        type : mongoose.Schema.Types.ObjectId,
        required : true,
        ref : 'Customer'
    },
    shipping_address :  {},
    billing_address : {},
    products : [
        {
            product_id : String,
            quantity : Number,
            name : String,
            price : Number
        }
    ],
    order_total : { type : Number },
    payment_method : { type : String },
    payment_status : { type : String },
    order_status : { type : String },
    active : {
        type : Boolean,
        default : true
    },
    modified_on : {
        type  : Date,
        default : Date.now()
    }

},
{ timestamps: true }
);

module.exports = mongoose.model('Order',orderSchema);