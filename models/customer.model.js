const mongoose = require("mongoose")

const customerSchema = mongoose.Schema({

    first_name : {
        type : String,
        required : true
    },
    last_name : {
        type : String,
        required : true
    },
    email : {
        type : String,
        required : true
    },
    password : {
        type : String,
        required : true
    },
    mobile : {
        type : String,
        required : true
    },
    gender : {
        type : String
    },
    dob : {
        type : String
    },
    billing_address : {},
    delivery_address : {},
    active : {
        type : Boolean,
        default : true
    },
    modified_on : {
        type  : Date,
        default : Date.now()
    }
},
{timestamps : true}

);


module.exports = mongoose.model("Customer",customerSchema)
