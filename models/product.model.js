const mongoose = require("mongoose");


const productLooseTypeDataSchema = mongoose.Schema({
    title : {
        type : String
    },
    price : {
        type : Number
    },
    weight : {
        type : Number
    }
});

const productSchema = mongoose.Schema({
    title : {
        type : String,
        required : true
    },
    sku : {
        type : String,
    },
    description : {
        type : String,
    },
    features_and_details : {
        type : String,
    },
    product_information : {
        type : []
    },
    category : {
        type : mongoose.Schema.Types.ObjectId,
        required : true,
        ref : 'Category'
    },
    sub_category : {
        type : mongoose.Schema.Types.ObjectId,
        required : true,
        ref : 'Subcategory'
    },
    images : {
        type : [],
    },
    quantiy_available  : {
        type : Number,
        required : true
    },
    base_price : {
        type :Number,
        required : true
    },
    offer_price : {
        type :Number,
        required : true
    },
    delivery_information : {
        type : String,
    },
    created : {
        type : Date,
        default : Date.now
    },
    updated : {
        type : Date
    }

});

module.exports = mongoose.model('Product',productSchema);