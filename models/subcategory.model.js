const mongoose = require("mongoose");

const subcategorySchema = mongoose.Schema({

	sub_category_name : {
		type :String,
		required : true
	},
	category : {
		type : mongoose.Schema.Types.ObjectId,
		required : true,
		ref : 'Category'
	},
	created : {
        type : Date,
        default : Date.now
    },
    updated : {
        type : Date
    }

});

module.exports = mongoose.model('Subcategory',subcategorySchema);