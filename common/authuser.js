const User = require("../models/user.model")
const userAuthentication = async function (req, res, next) {

  let auth_key = req.headers['auth-key'];

  if(!auth_key){

    return res.status(404).send({
      status : 3,
      message : "Authentication key required",
      data : []
    });

  }

  //find user exist 
  let user = await User.findOne({ token_id : auth_key });

  if(!user){
    return res.status(404).send({
      status : 3,
      message : "Invalid user",
      data : []
    });
  }

  console.log('user valid')
  next()
}

module.exports = userAuthentication