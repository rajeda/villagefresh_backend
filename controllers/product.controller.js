const Product = require("../models/product.model");


exports.create = async (req,res) => {

    const product = new Product({
        title : req.body.title,
        sku : req.body.sku,
        description : req.body.description,
        // features_and_details : req.body.features_and_details,
        category : req.body.category,
        sub_category : req.body.sub_category,
        delivery_information : req.body.delivery_information,
        quantiy_available : req.body.quantiy_available,
        base_price : req.body.base_price,
        offer_price : req.body.offer_price
    });

    var product_images = req.files.product_images;
    var product_imaage_arr = [];
    if(product_images.length > 1) {
        product_images.forEach( val => {
            console.log(val)
            var image_name = val.name.replace(/ /g,"_");
            image_name = Date.now() +  "_" + image_name;
            console.log(image_name);

            var hostname = req.headers.host; // hostname = 'localhost:8080'
            val.mv('./uploads/products/' + image_name);
            product_imaage_arr.push(
                {
                    base_url : hostname,
                    image_folder : "products",
                    image_name : image_name
                }
            );
        });

    }else{
        var image_name = product_images.name.replace(/ /g,"_");
        image_name = Date.now() +  "_" + image_name;
        var hostname = req.headers.host; // hostname = 'localhost:8080'
        product_images.mv('./uploads/products/' + image_name);
        product_imaage_arr.push(
            {
                base_url : hostname,
                image_folder : "products",
                image_name : image_name
            }
        );
    }

    product.images = product_imaage_arr;

    try{
        const productcreate = await product.save();
        return res.status(200).send({
            status : 1,
            message : "success",
            data : productcreate
        });
    }catch(error) {
        return res.status(400).send({
            status : 0,
            message : error,
            data : []
        });
    }


};


exports.findAll = async (req,res) => {

    try {

        let skip = parseInt(req.query.skip);
        let limit = parseInt(req.query.limit);


        const products  = await Product.find()
                                    .skip(skip)
                                    .limit(limit)
                                    .exec();

                                    
        return res.status(200).send({
            status : 1,
            message : "success",
            data : products
        });
    }catch(error) {
        return res.status(400).send({
            status : 0,
            message : error,
            data : []
        });
    }

}

exports.findOne = async (req,res) => {

    try {
        var product_id = req.params.id;
        const products  = await Product.findById(product_id)
        .populate('category')
        .populate('sub_category',)
        .exec();;
        return res.status(200).send({
            status : 1,
            message : "success",
            data : products
        });
    }catch(error) {
        return res.status(400).send({
            status : 0,
            message : error,
            data : []
        });
    }

};






exports.update = async (req,res) => {

    var product_tbl = await Product.findById(req.body.product_id);

    product_tbl.title = req.body.title;
    product_tbl.sku = req.body.sku;
    product_tbl.description = req.body.description;
    product_tbl.category = req.body.category;
    product_tbl.sub_category = req.body.sub_category;
    product_tbl.delivery_information = req.body.delivery_information;
    product_tbl.quantiy_available = req.body.quantiy_available;
    product_tbl.base_price = req.body.base_price;
    product_tbl.offer_price = req.body.offer_price;

    if(req.files){

        var product_images = req.files.product_images;
    
        if(product_images.length > 1) {
            product_images.forEach( val => {
                console.log(val)
                var image_name = val.name.replace(/ /g,"_");
                image_name = Date.now() +  "_" + image_name;
                console.log(image_name);
    
                var hostname = req.headers.host; // hostname = 'localhost:8080'
                val.mv('./uploads/products/' + image_name);
                product_tbl.images.push({
                    base_url : hostname,
                    image_folder : "products",
                    image_name : image_name
                });
    
            });
    
        }else{
            var image_name = product_images.name.replace(/ /g,"_");
            image_name = Date.now() +  "_" + image_name;
            var hostname = req.headers.host; // hostname = 'localhost:8080'
            product_images.mv('./uploads/products/' + image_name);
            product_tbl.images.push({
                base_url : hostname,
                image_folder : "products",
                image_name : image_name
            });
    
        }

    }
   

    try{
        const productupdated = await product_tbl.save();
        return res.status(200).send({
            status : 1,
            message : "success",
            data : productupdated
        });
    }catch(error) {
        return res.status(400).send({
            status : 0,
            message : error,
            data : []
        });
    }


};

exports.remove = async (req,res) => {

    try {
        var product_id = req.params.productid;
        const products  = await Product.deleteOne({ _id : product_id});

        if(products){

            return res.status(200).send({
                status : 1,
                message : "Success",
                data : products
            });

        }


    }catch(error) {
        return res.status(400).send({
            status : 0,
            message : error,
            data : []
        });
    }


};

exports.removeImage  = async (req,res) => {


    try {
        var product_id = req.body.product_id;
        var index_id = req.body.index;
        const products  = await Product.findById(product_id);

        if(products){

            var product_images = products.images;

            var new_product_images = product_images.filter( (val,index) => {
                return index !== index_id;
            });


            products.images = new_product_images;
            const productupdated = await products.save();

            if(productupdated){

                return res.status(200).send({
                    status : 1,
                    message : "Success",
                    data : products
                });

            }



        }


    }catch(error) {
        return res.status(400).send({
            status : 0,
            message : error,
            data : []
        });
    }

};


exports.findAllCount = async (req,res) => {

    try{

		
		
		const products =  await Product.estimatedDocumentCount();

		return res.status(200).send({
			status : 1,
			message : "Success",
			data : products
		});


	}catch(error) {

		res.status(400).send({
			status : 0,
			message : error,
			data : []
		})
	}
}