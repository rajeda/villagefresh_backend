const Order = require("../models/order.model");
const Cart = require("../models/cart.model");


exports.create = async (req,res) => {

    try{

        const { user_id , shipping_address , billing_address ,payment_method , payment_status} = req.body;

        //get  user cart details
        let cart_details = await Cart.findOne({ user_id });

        if(cart_details){

            let cart_product_data = cart_details.products;

            let order_total = 0;
            cart_product_data.forEach(product => {
                order_total = order_total + (product.quantity * product.price);
            });


            let newOrder = new Order(
                {
                    user_id : user_id,
                    shipping_address : shipping_address,
                    billing_address : billing_address,
                    products : cart_product_data,
                    order_total : order_total,
                    payment_method : payment_method,
                    payment_status : payment_status,
                    order_status : "Pending",
        
                }
            );
            newOrder = await newOrder.save();
    
            return res.status(200).send({
                status : 1,
                message : "success",
                data : newOrder
            });


        }else{
            return res.status(200).send({
                status : 1,
                message : "no cart data",
                data : newOrder
            });
        }



    }catch(error) {

         return res.status(400).send({
            status : 0,
            message : error,
            data : []
        });
    }


};



exports.findAll = async (req,res) => {

    try{

        let skip = parseInt(req.query.skip);
        let limit = parseInt(req.query.limit);

        //const orders = await Order.find();
        const orders = await Order.find()
                                    .populate('customer')
                                    .skip(skip)
                                    .limit(limit)
                                    .exec();

        console.log(orders)

        return res.status(200).send({
            status : 1,
            message : "success",
            data : orders
        });

    }catch(error) {

        return res.status(400).send({
            status : 0,
            message : error,
            data : []
        });
    }

};

exports.findOne = async (req,res) => {


    try{

        const { order_id } = req.params;

        const orders = await Order.findById(order_id).populate('customer').exec();

        return res.status(200).send({
            status : 1,
            message : "success",
            data : orders
        });
        
    }catch(error) {

        return res.status(400).send({
            status : 0,
            message : error,
            data : []
        });
    }
};

exports.update = async (req,res) => {

    
    try{

        const { status,order_id } = req.body;

        const orders = await Order.findById(order_id);

        if(orders){

            orders.order_status = status;
            let update_order = await orders.save();

            return res.status(200).send({
                status : 1,
                message : "success",
                data : update_order
            });
        }


        
    }catch(error) {

        return res.status(400).send({
            status : 0,
            message : error,
            data : []
        });
    }
}



exports.findAllCount = async (req,res) => {

    try{

		
		
		const orders =  await Order.estimatedDocumentCount();

		return res.status(200).send({
			status : 1,
			message : "Success",
			data : orders
		});


	}catch(error) {

		res.status(400).send({
			status : 0,
			message : error,
			data : []
		})
	}
}