const Category = require("../models/category.model");
const Subcategory = require("../models/subcategory.model");


exports.create = async (req,res) => {

    try{

        const category = new Category({
            category_name : req.body.category_name
        });

        const categorySaved = await category.save();

        return res.status(200).send({
            status : 1,
            message : "success",
            data : categorySaved
        });

    }catch(error) {

        return res.status(400).send({
            status : 0,
            message : error,
            data : []
        });
    }

};

exports.update = async (req,res) => {

    try{

        // console.log(req.body.cat_id);
        // if(req.body.cat_id  == ""){

        //     return res.status(400).send({
        //         status : 0,
        //         message : "category id need",
        //         data : []
        //     });
    
        // }

        const categories = await Category.findById(req.body.cat_id);

        if(categories){

            categories.category_name = req.body.category_name;

            const categoryupdate = await categories.save();

            if(categoryupdate){

                        
                return res.status(200).send({
                    status : 1,
                    message : "success",
                    data : categoryupdate
                });

            }


        }


    }catch(error) {

        return res.status(400).send({
            status : 0,
            message : error,
            data : []
        });
    }

}


exports.findAllCount = async (req,res) => {

    try{

		
		
		const cateories =  await Category.estimatedDocumentCount();

		return res.status(200).send({
			status : 1,
			message : "Success",
			data : cateories
		});


	}catch(error) {

		res.status(400).send({
			status : 0,
			message : error,
			data : []
		})
	}
}

exports.findAll = async (req,res) => {

    try{
        console.log(req.query)
		let skip = parseInt(req.query.skip);
        let limit = parseInt(req.query.limit);

        console.log(skip)
        

        const categories = await Category.find()		
                                .skip(skip)
                                .limit(limit)
                                .exec();

        console.log("1")

        return res.status(200).send({
            status : 1,
            message : "success",
            data : categories
        });

    }catch(error) {
         return res.status(400).send({
            status : 0,
            message : error,
            data : []
        });
    }
};

exports.findOne  = async (req,res) => {

    try{

        var category_id = req.params.cat_id
        const categories = await Category.findById(category_id);

        if(categories){

            return res.status(200).send({
                status : 1,
                message : "success",
                data : categories
            });

        }



    }catch(err){
        return res.status(400).send({
            status : 0,
            message : err,
            data : []
        });
    }

};

exports.delete = async (req,res) => {

    try{

        var category_id = req.params.cat_id;

        const categories = await Category.deleteOne({ _id : category_id});

        return res.status(200).send({
            status : 1,
            message : "success",
            data : []
        });


    }catch(err){
        return res.status(400).send({
            status : 0,
            message : err,
            data : []
        });
    }

}


exports.findAllSubCategories = async (req,res) => {

    try{
        var category_id = req.params.cat_id

        console.log(category_id)

        const subcategory = await Subcategory.find({
            category : category_id
        });
        return res.status(200).send({
            status : 1,
            message : "success",
            data : subcategory
        })

    }catch(err) {

        return res.status(400).send({
            status : 0,
            message : err,
            data : []
        });

    }
}
