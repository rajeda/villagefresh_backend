const Cart = require("../models/cart.model");

exports.create = async (req,res) => {

    const { user_id, product_id, quantity, name, price } = req.body;


    try{
        // find user  has already added any cart
        let cart = await Cart.findOne({ user_id  });
        if(cart) {
            //cart exist for user
            let itemindex = cart.products.findIndex( p => p.product_id === product_id);

            if(itemindex > -1){
                //if product exist in the cart update
                let productitem = cart.products[itemindex];
                productitem.quantity = quantity;
                cart.products[productitem] = productitem;
            }else{
                //product does not exists in cart, add new item
                cart.products.push({
                    product_id,quantity,name,price
                });
            }

            cart = await cart.save();
            return res.status(200).send({
                status : 1,
                message : "success",
                data : cart
            });
        }else{
            console.log("newCart",Cart)

            //no cart for user, create new cart
            let  newCart =  new Cart({
                user_id,
                products : [{
                    product_id,quantity,name,price
                }]
            });

            createcart = await newCart.save();

            return res.status(200).send({
                status : 1,
                message : "success",
                data : createcart
            });
        }


    }catch(error) {

        return res.status(400).send({
            status : 0,
            message : error,
            data : []
        });
    }



};

exports.findAll = async (req,res) => {

    try{

        let  user_id = req.params.user_id;

        const cartData = await Cart.findOne({
            user_id
        });

        if(cartData){
            return res.status(200).send({
                status : 1,
                message : "success",
                data : cartData
            });
        }else{

            return res.status(200).send({
                status : 1,
                message : "no cart data",
                data : []
            });
        }

    }catch(error) {

        return res.status(400).send({
            status : 0,
            message : error,
            data : []
        });
    }
};

exports.removeCart = async (req,res) => {

    try{
        const { user_id, product_id } = req.body;

        let cart = await Cart.findOne({user_id});

        if(cart) {

            let updateProductData = cart.products.filter(
                prod =>  prod.product_id !== product_id
            );

            cart.products = updateProductData;

            let cartupdate = await cart.save();

            return res.status(200).send({
                status : 1,
                message : "success",
                data : cartupdate
            });

        }else{

            return res.status(200).send({
                status : 1,
                message : "no cart data",
                data : []
            });
        }
        
    }catch(error) {

        return res.status(400).send({
            status : 0,
            message : error,
            data : []
        });
    }

};


exports.updateProductQuantity = async (req,res) => {

    try{
        const { user_id, product_id , type } = req.body;

        let cart = await Cart.findOne({user_id});

        if(cart) {

            let itemindex = cart.products.findIndex( p => p.product_id === product_id);

            if(itemindex > -1){

                let productitem = cart.products[itemindex];
                if(type == "increment"){
                    productitem.quantity = productitem.quantity + 1;
                }

                if(type == "decrement"){
                    productitem.quantity =  productitem.quantity - 1;
                }
                cart.products[productitem] = productitem;
                cart = await cart.save();
                return res.status(200).send({
                    status : 1,
                    message : "success",
                    data : cart
                });

            }else{

                return res.status(200).send({
                    status : 1,
                    message : "no product data",
                    data : []
                });
            }


        }else{

            return res.status(200).send({
                status : 1,
                message : "no cart data",
                data : []
            });
        }
        
    }catch(error) {

        return res.status(400).send({
            status : 0,
            message : error,
            data : []
        });
    }
};