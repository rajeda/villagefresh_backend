const Subcategory = require("../models/subcategory.model");
const Category = require("../models/category.model");

exports.create = async (req,res) => {

	try{

		const sb_category = new Subcategory({

			sub_category_name : req.body.sub_category_name,
			category : req.body.category

		});


		const sub_cateory_save = await sb_category.save();
		return res.status(200).send({
			status : 1,
			message : "success",
			data : sub_cateory_save
		});

	}catch(error) {

		res.status(400).send({
			status : 0,
			message : error,
			data : []
		})
	}


};

exports.update = async (req,res) => {

	try{

		var sb_category = await Subcategory.findById(req.body.sub_cat_id);

		console.log(sb_category)

		if(sb_category){

			sb_category.sub_category_name = req.body.sub_category_name;
			sb_category.category = req.body.category;
			const sb_category_update = await sb_category.save();

			if(sb_category_update){

				return res.status(200).send({
					status : 1,
					message : "success",
					data : sb_category_update
				});

			}


		}



	}catch(error) {

		res.status(400).send({
			status : 0,
			message : error,
			data : []
		})
	}


};

exports.findAll = async (req,res) => {

	try{


		//const { skip , limit } = parseInt(req.params);
		let skip = parseInt(req.params.skip);
		let limit = parseInt(req.params.limit);
		const sub_cateories = await Subcategory
									.find({})
									.populate('category','category_name')
									.skip(skip)
									.limit(limit)
									.exec();
		return res.status(200).send({
			status : 1,
			message : "Success",
			data : sub_cateories
		});

	}catch(error) {

		res.status(400).send({
			status : 0,
			message : error,
			data : []
		})
	}
};


exports.findOne = async (req,res) => {


	try{
        var sub_cat_id = req.params.sub_cat_id;
		const sub_cateories = await Subcategory.findById(sub_cat_id);
		return res.status(200).send({
			status : 1,
			message : "Success",
			data : sub_cateories
		});

	}catch(error) {

		res.status(400).send({
			status : 0,
			message : error,
			data : []
		})
	}


};


exports.remove = async (req,res) => {


	try{

		var sub_cat_id = req.params.sub_cat_id;
		
		const sub_cateories =  await Subcategory.deleteOne({ _id : sub_cat_id});

		return res.status(200).send({
			status : 1,
			message : "Success",
			data : sub_cateories
		});


	}catch(error) {

		res.status(400).send({
			status : 0,
			message : error,
			data : []
		})
	}


};


exports.findAllCount = async(req,res) => {

	try{

		
		
		const sub_cateories =  await Subcategory.estimatedDocumentCount();

		return res.status(200).send({
			status : 1,
			message : "Success",
			data : sub_cateories
		});


	}catch(error) {

		res.status(400).send({
			status : 0,
			message : error,
			data : []
		})
	}


}


