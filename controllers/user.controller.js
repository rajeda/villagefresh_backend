const User  = require("../models/user.model")
const crypto = require('crypto');


exports.register = async (req,res) => {

    try{

        const { username , email , password } = req.body;

        let user_exist = await User.findOne({ email });

        if(user_exist){

            return res.status(200).send({
                status : 2,
                message : "email already exists",
                data : []
            });

        }

        let user = new User({ username , email , password })

        let user_save = await user.save();

        return res.status(200).send({
            status : 1,
            message : "success",
            data : user_save
        })

    }catch(error){
        return res.status(400).send({
            status : 0,
            message : error,
            data : []
        })
    }

};

exports.login = async (req,res) => {
    
    try{

        // console.log(req.headers['auth-key'])
        // return;

        const {  email , password } = req.body;

        //email checking

        let email_exist = await User.findOne({ email });

        if(!email_exist){

            return res.status(200).send({
                status : 2,
                message : "invalid email address",
                data : []
            })
        }

        let password_exist = await User.findOne({ password });

        if(!password_exist){

            return res.status(200).send({
                status : 2,
                message : "incorrect password",
                data : []
            })
        }

        let user = await User.findOne({ email : email , password : password });
        console.log(user)

        //generate token
        try{

            console.log(user)

            let cryptic = await crypto.randomBytes(256);
            let token = cryptic.toString('hex');
            user.token_id = token;

            let user_save = await user.save();

            return res.status(200).send({
                status : 1,
                message : "successfully logined",
                data : user_save.token_id
            });
            
        }catch(error){

            return res.status(400).send({
                status : 0,
                message : error,
                data : []
            })

        }

    }catch(error){

        return res.status(400).send({
            status : 0,
            message : error,
            data : []
        })
    }

};