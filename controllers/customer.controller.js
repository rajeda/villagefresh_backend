const Customer = require("../models/customer.model")

exports.register = async (req,res) => {
    

    try {

        const { first_name , last_name , email , password , mobile , gender , dob } = req.body;

        //checking email already exist

        let customer_exist = await Customer.findOne({ email : email });

        if(customer_exist) {

            return res.status(200).send({
                status : 2,
                message : "email already exists",
                data : []
            });

        }else{

            console.log("customer")

            let customer = new Customer({
                first_name , last_name , email , password , mobile , gender , dob
            });

    
            customer = await customer.save();
    
            return res.status(200).send({
                status : 1,
                message : "success",
                data : customer
            });

        }



    }catch(error){

        return res.status(400).send({
            status : 0,
            message : error,
            data : [] 
        });
    }

};


exports.findAll = async (req,res) => {

    try{

        let skip = parseInt(req.query.skip);
        let limit = parseInt(req.query.limit);

        const customers = await Customer.find()
        .skip(skip)
        .limit(limit)
        .exec();

        return res.status(200).send({
            status : 1,
            message : "success",
            data : customers
        });

    }catch(error) {
        return res.status(400).send({
            status : 0,
            message : error,
            data : [] 
        });
    }

};


exports.findOne = async (req,res) => {

    try{
        const { customer_id } = req.params;

        const customer = await  Customer.findOne({ _id : customer_id });

        return res.status(200).send({
            status : 1,
            message : "success",
            data : customer
        });

    }catch(error) {

        return res.status(400).send({

            status : 0,
            message : error,
            data : [] 
        })
    }

};


exports.login = async (req,res) => {

    

};


exports.findAllCount = async (req,res) => {

    try{

		
		
		const customers =  await Customer.estimatedDocumentCount();

		return res.status(200).send({
			status : 1,
			message : "Success",
			data : customers
		});


	}catch(error) {

		res.status(400).send({
			status : 0,
			message : error,
			data : []
		})
	}
}