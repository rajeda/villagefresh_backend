//mongodb://rajesh:Rajesh123@ds131296.mlab.com:31296/villagefresh
const express = require('express');
const app = express();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const fileUpload = require('express-fileupload');
const cors = require("cors");
require('dotenv/config');
const userAuth = require("./common/authuser")
const path = require('path');

//middlewares
// enable files upload
app.use(cors());
app.use(fileUpload({
  createParentPath: true
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); 
// app.use(express.static('uploads'));
app.use(express.static(__dirname + '/uploads'));

//import Routes
const postsRoute = require('./routes/posts');
const productRoute = require('./routes/products');
const categoryRoute = require('./routes/categories');
const subCategoryRoute = require('./routes/sub_categories');
const cartRoute = require('./routes/cart');
const orderRoute = require("./routes/order");
const customerRoute = require("./routes/customer");
const userRoute = require("./routes/user");

app.use("/api/posts",userAuth,postsRoute);
app.use("/api/products",userAuth,productRoute);
app.use("/api/categories",userAuth,categoryRoute);
app.use("/api/sub-categories",userAuth,subCategoryRoute);
app.use("/api/cart",userAuth,cartRoute);
app.use("/api/orders",userAuth,orderRoute);
app.use("/api/customers",customerRoute);
app.use("/api/users",userRoute);

app.use(express.static(path.join(__dirname, 'admin_frontend/build')));


app.get("/*" , (req,res) => {
  res.sendFile(path.join(__dirname, 'admin_frontend/build', 'index.html'));
});

//connect to DB
mongoose.connect(
  process.env.DB_CONNECTION,
  { useUnifiedTopology: true , useNewUrlParser: true} ,
  () => { console.log("connected to DB");}
  );

app.listen(5000, () =>
  console.log('Example app listening on port 5000!'),
);