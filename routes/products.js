const express = require("express");
const router = express.Router();
const productController = require("../controllers/product.controller");

router.post('/imageremove', productController.removeImage);
router.post('/', productController.create);
router.get('/', productController.findAll);
router.get('/count',productController.findAllCount);
router.get('/:id', productController.findOne);
router.put('/', productController.update);
router.delete('/:productid', productController.remove);

module.exports = router;