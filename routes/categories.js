const express = require("express");
const router  = express.Router();
const cateogryController = require("../controllers/category.controller");



router.post('/',cateogryController.create);
router.put('/',cateogryController.update);
router.get('/',cateogryController.findAll);
router.get('/count',cateogryController.findAllCount);


router.get('/:cat_id',cateogryController.findOne);
router.get('/sub-categories/list/:cat_id',cateogryController.findAllSubCategories);
router.delete('/:cat_id',cateogryController.delete);

module.exports = router;