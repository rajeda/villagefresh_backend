const express = require("express");
const router = express.Router();
const cartcController = require("../controllers/cart.controller")

router.post('/',cartcController.create)
router.get('/:user_id',cartcController.findAll)
router.post('/delete-cart',cartcController.removeCart)
router.post('/product-quantity-update',cartcController.updateProductQuantity)

module.exports = router;