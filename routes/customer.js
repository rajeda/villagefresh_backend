const express = require("express");
const router  = express.Router();
const customerController = require("../controllers/customer.controller")

router.post("/register",customerController.register);
router.post("/login",customerController.login);
router.get("/",customerController.findAll);
router.get('/customer/count',customerController.findAllCount);
router.get("/:customer_id",customerController.findOne);


module.exports = router;