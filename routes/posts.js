const express = require("express");
const router = express.Router();
const Post = require('../models/Post');

router.get('/',async (req,res) => {

    try{ 
        const posts = await Post.find({});
        res.status(200).send(posts);

    }catch(err) {
        res.status(400).send({message : err});

    }
    

});

router.post('/' , async (req,res) => {

   const post = new Post({
       title : req.body.title,
       description : req.body.description
   }); 

   try {
    const savePost = await post.save();
    res.status(200).send(savePost);
   }catch(err) {
    res.status(400).send({message : err});
   }


});


module.exports = router;