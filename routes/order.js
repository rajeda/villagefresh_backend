const express = require("express")
const router = express.Router()
const orderController = require("../controllers/order.controller")

router.post("/",orderController.create);
router.get("/",orderController.findAll);
router.post("/update-status",orderController.update);
router.get('/order/count',orderController.findAllCount);
router.get("/:order_id",orderController.findOne);

module.exports = router;