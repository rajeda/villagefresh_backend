const express = require("express");
const router = express.Router();
const subcategory = require("../controllers/sub_category.controller");

router.post("/",subcategory.create);
router.put("/",subcategory.update);
router.get("/fetch-all/:skip/:limit",subcategory.findAll);
router.get("/fetch-count",subcategory.findAllCount);
router.get("/fetch-one/:sub_cat_id",subcategory.findOne);
router.delete("/:sub_cat_id",subcategory.remove);

module.exports = router;